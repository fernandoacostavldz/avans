<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Puertas Plegadizas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puertas Plegadizas
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PuertasIII/570x300/pp-puertas-plegadizas.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PuertasIII/570x300/pp-puertas-plegadizas.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Las puertas plegadizas son perfectas para lugares donde no se cuenta con tanto espacio, mas sin embargo se desea un claro libre transitable amplio.
                                    </p>
                                    <p>
                                        Se presentan puertas plegadizas de dos o cuatro hojas para un flujo bidireccional sin problemas. La puerta plegadiza también cuenta con un sistema antipánico que permite abatir las puertas en caso de una emergencia.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-marco-aluminio.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_deslizantes.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_deslizantes.php">
                            <p class="post-title base-text-color">
                                Puertas Deslizantes
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-abatible.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_abatibles.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_abatibles.php">
                            <p class="post-title base-text-color">
                                Puertas Abatibles
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-curva.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_curvas.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_curvas.php">
                            <p class="post-title base-text-color">
                                Puertas Curvas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-giratoria.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Puertas Giratorias
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-plegadiza.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_plegadizas.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_plegadizas.php">
                            <p class="post-title base-text-color">
                                Puertas Plegadizas
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
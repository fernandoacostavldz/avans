<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyectos elevadores
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Elevadores
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Elevadores
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <br>
        <div class="container">
            <div class="row">
                <!-- Torre TOP -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="torre_top.php">
                                <div>Torre T.OP</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/top/top_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Considerada la torre más alta de latinoamérica (304m de altura) hacen de este
                                    proyecto un gran reto de ingeniería, en transportación vertical
                                </p>
                            </a>
                        </div>
                    </div>

                <!-- NEST -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="nest.php">
                                <div>Torre NEST</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nest/n_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Torre de 36 niveles de uso habitacional ubicada en el mismo predio que la Torre T.OP, unidas por 12 niveles 
de estacionamiento
                                </p>
                            </a>
                        </div>
                    </div>
    
            </div>
            <div class="row">
                
                <!-- Nave 01  -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="nave01.php">
                                <div>Nave 01</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave_01/n_570_2.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Complejo de usos mixtos con zona comercial abierta y estacionamiento compartido con
importantes cadenas de restaurantes
                                </p>
                            </a>
                        </div>
                    </div>
                
                <!-- PUNTACERO -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="puntacero.php">
                                <div>PUNTACERO</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/puntacero/pc_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Elevadores y escaleras eléctricas dan la movilidad a este proyecto. Ubicado en una de las zonas mas 
importantes de Monterrey
                                </p>
                            </a>
                        </div>
                    </div>
            </div>
            <div class="row">

                <!-- CITICA -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="citica.php">
                                <div>Citica</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/citica/c_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Torre de departamentos con áreas de oficinas y comercio independientes.
                                </p>
                            </a>
                        </div>
                    </div>
                <!--Centrika Elite -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="torre_centrika.php">
                                <div>Torre Céntrika Elite</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/centrika/ce_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Complejo habiltacional con especial atención a la comodidad de los inquilinos al contar con un elevador de pasajeros y otro de servicio diseñado especialmente para mudanzas
                                </p>
                            </a>
                        </div>
                    </div>
    
            </div>

            <div class="row">
                <!-- Pabellón M -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="PabellonM.php">
                                <div>Pabellón M</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/pabellon_m/pm_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    En este proyecto instalamos 2 elevadores de alta capacidad de carga (6,000 kg) que ayudan a mover grandes escenarios y  equipo pesado dentro del auditorio de Pabellón M 
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Moll del Valle -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="moll_valle.php">
                                <div>Moll del Valle</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/moll_del_valle/mv_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Desarrollo dividido en 2 áreas de distintas etapas, unidos en su transportación por una solución vertical panorámica
                                </p>
                            </a>
                        </div>
                    </div>
            </div>

            <div class="row">

                <!-- Colegio inglés -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="colegio_ingles.php">
                                <div>Colegio Inglés</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/colegio_ingles/ci2_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    La accesibilidad para todos los estudiantes y personal administrativo es muy importante para el Colegio Inglés, por lo que decidieron instalar un elevador de pasajeros, cómodo, seguro y muy transparente.
                                </p>
                            </a>
                        </div>
                    </div>

                <!-- Punto Lomas -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="punto_lomas.php">
                                <div>Punto Lomas</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/punto_lomas/pl_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Diseño de elevadores a la medida en dimensiones y en estructura sin tener que sacrificar la eficacia en la transportación vertical
                                </p>
                            </a>
                        </div>
                    </div>
                    
            </div>
            <div class="row">
                <!-- TULÉ -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                        <div class="project-item post-item">
                            <a  class="square-link" href="tule.php">
                                <div>Tulé</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/tule/t_570_2.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Complejo habitacional formado por 3 edificios con estacionamiento compartido
                                </p>
                            </a>
                        </div>
                    </div>
            </div>

                <!-- Plaza Volga FOTOS PENDIENTES -->
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="plaza_volga.php">
                                <div>Plaza Volga</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/PLAZA VOLGA/plaza volga.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    En este proyecto instalamos elevadores para la plaza comercial que cuenta con tres niveles de locales y 4 sótanos de estacionamiento.
                                </p>
                            </a>
                        </div>
                    </div> -->

                <!-- Teatro de la Ciudad FOTOS PENDIENTES-->
                   <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="nave01.php">
                                <div>Teatro de la Ciudad</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave01.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Montacargas diseñado para transportar grandes volúmenes de carga, como: escenografías, equipos de iluminación y sonido, vehículos, etc. porque el espectáculo debe de continuar.
                                </p>
                            </a>
                        </div>
                    </div> -->

                <!--Portenntum FOTOS PENDIENTES-->
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="nave01.php">
                                <div>Portenntum</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave01.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    La transportación vertical que necesita una bodega, para grandes volúmenes de carga, con un tráfico vertical intenso.
                                </p>
                            </a>
                        </div>
                    </div> -->

                <!-- Torre Galia FOTOS PENDIENTES -->
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="torre_galia.php">
                                <div>Torre Galia</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/GALIA/galia_570.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Por su diseño estructural este edificio necesitaba transportación vertical a la medida, pero sin perder el glamour que ofrece un elevador panorámico. 
                                </p>
                            </a>
                        </div>
                    </div> -->

                <!-- Livin FOTOS PENDIENTES -->
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="torre_livin.php">
                                <div>Livin</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave01.jpg"/>
                                    </div>
                                </div>
                                <p>
                                En este edificio se instalaron elevadores adaptables a las dimensiones del diseño arquitectónico. 
                                </p>
                            </a>
                        </div>
                    </div> -->

                <!-- Plaza California FOTOS PENDIENTES -->
                    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a  class="square-link" href="plaza_california.php">
                                <div>Plaza California</div>
                                <div class="img-container">
                                    <div class='inner'>
                                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave01.jpg"/>
                                    </div>
                                </div>
                                <p>
                                    Para facilitar la transportación de los insumos para los restaurantes y el área comercial de esta plaza, fue necesaria la instalación de un elevador de servicio.
                                </p>
                            </a>
                        </div>
                    </div> -->
        </div>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta deslizante:
                        </b>
                        Totalmente de Cristal
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-7.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/puertas-automaticas-totalmente-de-cristal-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/totalmente-de-cristal/totalmente-de-cristal-7.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        La puerta CGL LT es nuestro modelo minimalista en su diseño. Diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia. Los vidrios templados con cantos pulidos y abrillantados de forma perimetral están unidos a hueso contra el piso y costados, lo que da una sensación de transparencia y modernidad incomparable.
                                    </p>
                                    <p>
                                        La puerta CGL LT cuenta con una batería de respaldo que acciona la puerta en caso de falta de energía y un cerrojo electrónico no visible totalmente en línea con la estética limpia del sistema.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        Montura superficial
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-sup/PA_deslizante_totalementedecristal_superficial_2.9.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-sup/AC_PA_deslizante_totalementedecristal_superficial_2.9.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-sup/PA_deslzizante_totalmentedecristal_superficial_2.12.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-sup/AC_PA_deslzizante_totalmentedecristal_superficial_2.12.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                    <p>
                                        Montura dentro del marco
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-d-marco/PA_deslizante_totalmentedecristal_dentromarco_2.3.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-d-marco/AC_PA_deslizante_totalmentedecristal_dentromarco_2.3.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-d-marco/PA_deslizante_totalmentedecristal_dentromarco_2.6.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/t-cristal/mon-d-marco/AC_PA_deslizante_totalmentedecristal_dentromarco_2.6.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Totalmente de Cristal (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otras_puertas.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
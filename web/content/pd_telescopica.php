<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Telescópica
    </title>
    <?php include 'includes/scripts_top.php';  ?>
    <body>
        <?php include 'includes/header.php';  ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta deslizante:
                        </b>
                        Telescópica
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770X600/DEZLIZANTES/telescopica/telescopicas-0.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/telescopica/telescopicas-0.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        La puerta CGL LT es nuestro modelo minimalista en su diseño. Diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia. Los vidrios templados con cantos pulidos y abrillantados de forma perimetral están unidos a hueso contra el piso y costados, lo que da una sensación de transparencia y modernidad incomparable.
                                    </p>
                                    <p>
                                        La puerta CGL LT cuenta con una batería de respaldo que acciona la puerta en caso de falta de energía y un cerrojo electrónico no visible totalmente en línea con la estética limpia del sistema.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        Montura superficial
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-sup/PA_deslizante_telescopica_superficial_3.3.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-X-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-sup/AC_PA_deslizante_telescopica_superficial_3.3.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-X-SX (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-sup/PA_deslizante_telescopica_superficial_3.4.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble P-X-SX-SX-X-P (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-sup/AC_PA_deslizante_telescopica_superficial_3.4.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble P-X-SX-SX-X-P (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                    <p>
                                        Montura dentro del marco
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-d-marco/PA_deslizante_telescopica_dentromarco_3.1.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC SO-SX-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-d-marco/AC_PA_deslizante_telescopica_dentromarco_3.1.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC SO-SX-SX (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-d-marco/PA_deslizante_telescopica_dentromarco_3.2.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC SO-SX-SX-SX-SX-SO (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/telescopicas/mon-d-marco/AC_PA_deslizante_telescopica_dentromarco_3.2.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC SO-SX-SX-SX-SX-SO (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otras_puertas.php'; ?>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
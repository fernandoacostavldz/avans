<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Equus335
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Equus 335
                        </b>
                    </h2>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/Equus335/wide9.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus335/wide5.jpg">
                </div>
            </div>
        </section>

        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p class="project-title">
                                        Servicios
                                    </p>
                                    <ul class="tags-list">
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Cliente
                                    </p>
                                    <p>
                                        Ejemplo
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Proyecto
                            </h2>
                            <p>
                                Ubicado en San Pedro Garza García, en Nuevo León, en una exclusiva zona corporativa se encuentra el proyecto Equus 335, el cual está ubicado cerca de avenidas importantes, centros comerciales y restaurantes.
                            </p>
                            <p>
                                Este edificio alberga oficinas AAA, teniendo importantes clientes que buscan lo mejor en eficiencia, seguridad y sobre todo exclusividad.
                            </p>
                            <p>
                                Muchas personas utilizan los accesos diariamente, por lo cual se instalaron puertas automáticas giratorias para que clientes y personas que laboran dentro de las oficinas puedan salir y entrar de una manera fácil y óptima.
                            </p>
                            <p>
                                En los accesos del estacionamiento, las personas pueden abrir las puertas automáticas por medio de un sistema de seguridad, lo cual evita que personas que no cuentan con el acceso adecuado, entren al edificio. Dentro de este proyecto también se instalaron torniquetes de la más alta tecnología, los cuales ayudan al control de personas que ingresan al edificio evitando que personas ajenas o que carezcan de los accesos necesarios puedan acceder libremente por los espacios del edificio.
                            </p>
                            <p>
                                Para este proyecto, se utilizaron puertas automáticas giratorias completamente de cristal con operador subterráneo, modelo TQ Crystal. Una característica de estos equipos es que cuentan con abatimiento por emergencia en todas sus hojas. Las unidades son de importación, se fabrican en Holanda.
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otros Proyectos
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Live_Aqua.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Live_Aqua.php">
                            <p class="post-title base-text-color">
                                Grand Fiesta Americana
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Elevadores
                            </li>
                            <li>
                                Puertas Autómaticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Fashion_Drive.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Fashion_Drive.php">
                            <p class="post-title base-text-color">
                                Fashion Drive
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Arboleda.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Arboleda.php">
                            <p class="post-title base-text-color">
                                Arboleda
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Restaurante.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Restaurante.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Restaurante.php">
                            <p class="post-title base-text-color">
                                Restaurante
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus335.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus335.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus335.php">
                            <p class="post-title base-text-color">
                                Equus 335
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/HolyCow.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Holy_Cow.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Holy_Cow.php">
                            <p class="post-title base-text-color">
                                Holy Cow
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus444.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus444.php">
                            <p class="post-title base-text-color">
                                Equus 444
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Trebol_Park.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Trebol_Park.php">
                            <p class="post-title base-text-color">
                                Trebol Park
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/PabellonM.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="PabellonM.php">
                                </a>
                            </div>
                        </figure>
                        <a href="PabellonM.php">
                            <p class="post-title base-text-color">
                                Pabellón M
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts_bottom.php"; ?>
    <?php include "chat.php"; ?>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyectos puertas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puertas automáticas
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Puertas
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <br>
        <div class="container">
             
            <div class="row">
                <!-- Andamar LifeStyle Center -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="andamar.php">
                                <div>
                                    Andamar LifeStyle Center
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/andamar/andamar1.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Para un centro comercial el tipo de puertas que se instalen son muy importantes, pues son las encargadas de darles la bienvenida a los consumidores, de mantener el aire acondicionado dentro del edificio y de mantener las medidas de seguridad.
                                </p>
                            </a>
                        </div>             
                    </div>
                <!-- Grand Fiesta Americana-->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Live_Aqua.php">
                                <div>Grand Fiesta Americana</div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/Live_Aqua/live_aqua_570.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Puertas automáticas que refuerzan el glamour y la modernidad del edificio, además ayudan a disminuir la entrada de aire caliente, lo que permite un ahorro de energía.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>
            
            <div class="row">
                <!-- Auditorio Pabellon M -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                        <div class="project-item post-item">
                            <a class="square-link" href="p_pabellonm.php"> 
                                <div>Pabellón M</div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/pabellonm/pabellonm_570x300.jpg"/>
                                        <!-- <div class="post-item__description"></div> -->
                                    </div>
                                </div>
                                <p>
                                    Puerta deslizante contra huracanes capaz de resistir vientos de hasta 250 km/hr. La puerta tiene un alma de acero dentro de los perfiles de aluminio cuentan con junquillos sólidos para mayor resistencia.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Equus 444 y 355 -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Equus.php">
                                <div>
                                    Equus 444 y 335
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/equus/e1_570.jpg"/>
                                    </div>
                                    
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                   Accesibilidad completa con puertas giratorias y torniquetes que previenen la entrada de personas ajenas al edificio
                                </p>
                            </a>
                        </div>
                    </div>

            </div>

            <div class="row">
                <!-- Arboleda -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Arboleda.php">
                                <div>
                                    Arboleda
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/arboleda/arboleda570_2.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    En este proyecto se utilizaron puertas automáticas con hojas de madera modelo Slide, montadas de manera superficial.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Trebol Park -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Trebol_Park.php">
                                <div>
                                    Trébol Park
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/Trebol_Park/wide6_590.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                   Entrada imponente con la más alta tecnología en los accesos con diseño vanguardista e innovador.
                                </p>
                            </a>
                        </div>
                    </div>                
            </div>

            <div class="row"> 
                <!-- Deportivo San Agustin -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="deportivo_san_agustin.php">
                                <div>
                                    Deportivo San Agustín
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/deportivo_san_agustin/deportivo1_590.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    En el Deportivo San Agustín los clientes necesitan puertas cómodas, elegantes, vanguardistas y funcionales.
                                </p>
                            </a>
                        </div>
                    </div>   
                <!-- Fashion Drive -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Fashion_Drive.php">
                                <div>Fashion Drive</div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/Fashion_Drive/wide4_590.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                   Sus puertas automáticas son capaces de soportar la gran afluencia de personas en todos sus niveles. 
                                </p>
                            </a>
                        </div>
                    </div>

            </div>

            <div class="row">
                <!-- Planta Skwinkles  -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="skwinkles.php">
                                <div>
                                    Planta Skwinkles
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/skwinkles/skwinkles1.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    La planta Skwinkles cuenta con puertas automáticas que garantizan la seguridad y la comodidad de la planta.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- The Capital Grille -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="capital_grille.php">
                                <div>The Capital Grille</div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/capital_grille/capital2.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Puertas automatizadas a la medida del cliente, que permiten conservar el estilo vanguardista del restaurante.
                                </p>
                            </a>
                        </div>         
                    </div>
            </div>

            <div class="row">
                <!-- Holy Cow -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="Holy_Cow.php">
                                <div>
                                    Holy Cow
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/holy_cow/holy_cow4_590.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->

                                </div>
                                <p>
                                    Accesos automáticos que benefician a los comensales porque pueden disfrutar del área exterior sin sufrir las inclemencias del clima; mientras que los meseros transportan la comida de forma rápida y sencilla.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- KIA Gonzalitos -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="kia_gonzalitos.php">
                                <div>
                                    KIA Gonzalitos
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/kia_gonzalitos/kia_570x300.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Para todos los edificios lo más importante es la fachada y junto con esta, la puerta que dará la bienvenida a los visitantes.
                                </p>
                            </a>
                        </div>             
                    </div>
            </div>

            <div class="row">
                <!-- Ecovía -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="ecovia.php">
                                <div>
                                    Ecovía
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/ecovia/EV3_530.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Se instalaron puertas automáticas deslizantes para entrar a las oficinas generales de Iza, cuentan con cerrojo electrónico y control de acceso a través de una tarjeta. 
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Iza Latitud Polanco -->
                   <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="iza_latitud.php">
                                <div>
                                    Iza Latitud Polanco
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/iza_latitud_polanco/ILP4_530.jpg"/>
                                    </div> -->
                                    <!-- <div class="post-item__description">
                                    </div> -->
                               <!--  </div>
                                <p>
                                    Se instalaron puertas automáticas deslizantes para entrar a las oficinas generales de Iza, cuentan con cerrojo electrónico y control de acceso a través de una tarjeta. 
                                </p>
                            </a>
                        </div>
                    </div> -->
                <!-- Torre KOI -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="torre_koi.php">
                                <div>
                                    Torre KOI
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/torre_koi/KOI_530.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Modernizar la entrada del edificio respetando el diseño del edificio, fue el reto que tuvo que enfrentar Avans al instalar puertas automáticas deslizantes.
                                </p>
                            </a>
                        </div>             
                    </div>
            </div>

            <div class="row">
                <!-- lowes -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="lowes.php">
                                <div>
                                    Lowes
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/lowes/L2_530.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->

                                </div>
                                <p>
                                    La tienda americana de mejoras para el hogar cuenta con 76 puertas automáticas deslizantes de Avans, en las sucursales que tiene en todo el país.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Deloitte  -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="deloitte.php">
                                <div>
                                    Deloitte 
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/deloitte/Deloitte2_530.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Modernizar la entrada del edificio respetando el diseño del edificio, fue el reto que tuvo que enfrentar Avans al instalar puertas automáticas deslizantes.
                                </p>
                            </a>
                        </div>             
                    </div>
            </div>

            <div class="row">
                <!-- Business HUB -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="business_hub.php">
                                <div>
                                    Business HUB
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/business_hub/BH_530.jpg  "/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->

                                </div>
                                <p>
                                    En este proyecto participamos en la remodelación del edificio, el cliente buscaba que los accesos a su edificio fuera más rápido, sin restarle modernidad
                                </p>
                            </a>
                        </div>
                    </div>
                    
                <!-- Clínica Nova (E5)  -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="clinica_nova.php">
                                <div>
                                    Clínica Nova (E5) 
                                </div>
                                <div class="img-container">
                                    <div class="inner">
                                        <img alt="single" class="img-responsive" src="images/proyectos/puertas/clinica_nova/CN_530.jpg"/>
                                    </div>
                                    <!-- <div class="post-item__description">
                                    </div> -->
                                </div>
                                <p>
                                    Para satisfacer las necesidades de funcionalidad, calidad y seguridad en los accesos de la Clínica Nova, Avans instaló puertas automáticas deslizantes en la entrada principal y puertas abatibles en los pasillos.
                                </p>
                            </a>
                        </div>             
                    </div>
            </div>
            <div class="row">
                
            </div>

        </div>
       
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
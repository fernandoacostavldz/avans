<?php
// hide the errors
error_reporting( 0 );

// Get the values from html form
$name    = $_POST['nombre'];
$email   = $_POST['email'];
$number  = $_POST['telefono'];
$message = $_POST['mensaje'];

// Email Address where you want to received the mails
$to = "ventas@avans.com";

// Mail Subject
$sub = "Información ventas AVANS";

// Output table
$email_message = '<html>
<head>
    <title>Enquiry From Your Productions Website</title>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-399268-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'UA-399268-1');
</script>
</head>
<body>
<table>
    <tr>
        <th align="left">EMAIL:</th>
        <td align="left">' . $email . '</td>
    </tr>
    <tr>
        <th align="left">NAME:</th>
        <td align="left">' . $name . '</td>
    </tr>
    <tr>
        <th align="left">PHONE:</th>
        <td align="left">' . $number . '</td>
    </tr>
    <tr>
        <th align="left">MESSAGE:</th>
        <td align="left">' . $message . '</td>
    </tr>
</table>
</body>
</html>
';

//Headers
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=UTF-8\r\n";
$headers .= "From: <" . $email . ">";

//send mail
$mail = mail( $to, $sub, $email_message, $headers );

if ( $mail ) {

    // Success message
    echo 'Mensaje enviado.';
} else {

    // Error message
    echo 'Error';
}
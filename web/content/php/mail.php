<?php 
/*------------------------------------------------------------------
Vanuva v1.0

Archivo        : mail.php
Descripción    : Envío de email
Versión        : 1.0
Autor          : Pablo Almaguer
Creación       : 15/06/2018

Contenido:
- Formulario de Contacto (función _get_data)
  - Declaración de variable email
  - Mensaje
    - Mensaje HTML
  - Función Wordwrap
  - Función mail

*****
* El formulario de contacto tiene el atributo
*  required por lo tanto las validaciones en PHP 
*  no son necesarias.
-------------------------------------------------------------------*/

ini_set('display_errors',0);

/*  Formulario de Contacto  */
function _get_data() {
  $name = $_POST['nombre'];
  $email = $_POST['email'];
  $number = $_POST['telefono'];
  $message = $_POST['mensaje'];

  return array($name, $email, $number, $message);
}

function _send_email() {
  /* Declaración de variable email  */
  $email_to = "ventas@avans.com, hola@avans.com";
  $email_subject = "Avans Contacto";
  $email_name = $_POST['nombre'];
  $email_from = $_POST['email'];
  $email_phone = $_POST['telefono'];
  $email_content = $_POST['mensaje'];
  $email_content_line = str_replace("\n", "<br>", $email_content);

  /* Encabezados de correo */
  $email_headers = 'MIME-Version: 1.0' . "\r\n";
  $email_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  // $email_headers .= 'To: Pablo Almaguer <' . $email_to . '>' . "\r\n";
  $email_headers .= 'From: ' . $email_name . ' <' . $email_from . '>' . "\r\n";

  /*  Mensaje HTML */
  $email_message = "
  <!DOCTYPE html>
  <html>
  <head>
    <title>" . $email_subject . "</title>
</head>
  <body>
    <h2><img src='" . $_SERVER['HTTP_HOST'] . "/images/logo_1.png' alt='Avans Contacto'></h2>
    <table>
    <tr>
      <th style='text-align: left;'>Nombre: </th>
      <td>" . $email_name . "</td>
    </tr>
    <tr>
      <th style='text-align: left;'>Correo: </th>
      <td>" . $email_from . "</td>
    </tr>
    <tr>
      <th style='text-align: left;'>Telefono: </th>
      <td>" . $email_phone . "</td>
    </tr>
    <tr>
      <th style='text-align: left;' valign='top'>Comentarios: </th>
      <td>" . $email_content_line . "</td>
    </tr>
  </table>
  </body>
  </html>
";
  
  mail($email_to, $email_subject, $email_message, $email_headers);
  echo "<script> alert('Mensaje Enviado') </script>";
}
if ($_POST)
  _send_email();
?>











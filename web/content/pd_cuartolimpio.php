<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Cuarto Limpio
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta deslizante:
                        </b>
                        Cuarto Limpio
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/cuarto-limpio/puertas-automaticas-cuarto-limpio-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        La puerta CGL LT es nuestro modelo minimalista en su diseño. Diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia. Los vidrios templados con cantos pulidos y abrillantados de forma perimetral están unidos a hueso contra el piso y costados, lo que da una sensación de transparencia y modernidad incomparable.
                                    </p>
                                    <p>
                                        La puerta CGL LT cuenta con una batería de respaldo que acciona la puerta en caso de falta de energía y un cerrojo electrónico no visible totalmente en línea con la estética limpia del sistema.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        Montura superficial
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-sup/PA_deslizante_cuartolimpio_superficial_7.3.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-X (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-sup/PA_deslizante_cuartolimpio_superficial_7.3.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-X (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-sup/PA_deslizante_cuartolimpio_superficial_7.4.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble SA P-X-X-P (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-sup/PA_deslizante_cuartolimpio_superficial_7.4.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble SA P-X-X-P (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                    <p>
                                        Montura dentro del marco
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-d-marco/PA_deslizante_cuartolimpio_dentromarco_7.1.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC O-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-d-marco/PA_deslizante_cuartolimpio_dentromarco_7.1.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC O-SX (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-d-marco/PA_deslizante_cuartolimpio_dentromarco_7.2.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC O-SX-SX-O (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/cuarto-limpio/mon-d-marco/PA_deslizante_cuartolimpio_dentromarco_7.1.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC O-SX-SX-O (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otras_puertas.php'; ?>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
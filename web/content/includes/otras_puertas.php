<section class="posts-container">
    <div class="container">
        <div class="section-title">
            <h2>
                Otras puertas
            </h2>
        </div>
        <div id="project-slide">
            <div class="project-item post-item">
                <a href="pd_marcoaluminio.php">
                    <p class="post-title base-text-color">
                        Marco de Aluminio
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-marco-aluminio.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_marcoaluminio.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_totalmentecristal.php">
                    <p class="post-title base-text-color">
                        Totalmente de Cristal
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-totalmente-cristal.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_totalmentecristal.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_zocloinferior.php">
                    <p class="post-title base-text-color">
                        Zoclo Inferior
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-zoclo-inferior.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_zocloinferior.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_telescopica.php">
                    <p class="post-title base-text-color">
                        Telescópica
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-telescopica.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_telescopica.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_macropuertas.php">
                    <p class="post-title base-text-color">
                        Macro Puertas
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-macropuertas.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_macropuertas.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_cuartolimpio">
                    <p class="post-title base-text-color">
                        Cuarto Limpio
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-cuarto-limpio.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_cuartolimpio.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_art.php">
                    <p class="post-title base-text-color">
                        Artísticas
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-artisticas1.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_art.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="pd_manuales.php">
                    <p class="post-title base-text-color">
                        Manuales ICU CCU
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-manuales-ICU-CCU1.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" data-gallery="f_project" href="pd_manuales.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
<!--             <div class="project-item post-item">
                <figure class="img-container">
                    <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-transporte-publico1.jpg"/>
                    <div class="post-item__description">
                        <a class="icon-plus" data-gallery="f_project" href="pd_tp.php">
                        </a>
                    </div>
                </figure>
                <a href="pd_tp.php">
                    <p class="post-title base-text-color">
                        Transporte Público
                    </p>
                </a>
            </div> -->
        </div>
    </div>
</section>
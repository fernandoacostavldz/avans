<section class="posts-container">
    <br>
    <div class="container">
        <div class="section-title">
            <h2>
                Otros Proyectos
            </h2>
        </div>
        <div id="project-slide">

            <!-- Andamar LifeStyle Center -->
                <div class="slide">
                    <a href="andamar.php">
                        <p class="post-title base-text-color">
                            Andamar LifeStyle Center
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/andamar/andamar1.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Grand fiesta Americana -->
                <div class="slide">
                    <a href="Live_Aqua.php">
                        <p class="post-title base-text-color">
                            Grand Fiesta Americana
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/Live_Aqua/live_aqua_570.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Pabellon M -->
                <div class="slide">
                    <a href="p_pabellonm.php">
                        <p class="post-title base-text-color">
                            Pabellón M
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/pabellonm/pabellonm_570x300.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Equus -->
                <div class="slide">
                    <a href="Equus.php">
                        <p class="post-title base-text-color">
                            Equus 444 y 335
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/equus/equus590.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Arboleda -->
                <div class="slide">
                    <a href="Arboleda.php">
                        <p class="post-title base-text-color">
                            Arboleda
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/arboleda/arboleda570_2.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Trebol Park -->
                <div class="slide">
                    <a href="Trebol_Park.php">
                        <p class="post-title base-text-color">
                            Trébol Park
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/Trebol_Park/wide6_590.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>   

            <!-- Deportivo San Agustín -->
                <div class="slide">
                    <a href="deportivo_san_agustin.php">
                        <p class="post-title base-text-color">
                            Deportivo San Agustín
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/deportivo_san_agustin/deportivo1_590.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Fashion Drive -->
                <div class="slide">
                    <a href="Fashion_Drive.php">
                        <p class="post-title base-text-color">
                            Fashion Drive
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/Fashion_Drive/wide4_590.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>
            
            <!-- Planta skwinkles -->
                <div class="slide">
                    <a href="skwinkles.php">
                        <p class="post-title base-text-color">
                            Planta Skwinkles
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/skwinkles/skwinkles1.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>
            
            <!-- Capital Grille -->
                <div class="slide">
                    <a href="capital_grille.php">
                        <p class="post-title base-text-color">
                            Capital Grille
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/capital_grille/capital2.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- Holy Cow -->
                <div class="slide">
                    <a href="Holy_Cow.php">
                        <p class="post-title base-text-color">
                            Holy Cow
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/holy_cow/holy_cow4_590.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>

            <!-- KIA Gonzalitos -->
                <div class="slide">
                    <a href="kia_gonzalitos.php">
                        <p class="post-title base-text-color">
                            KIA Gonzalitos
                        </p>
                        <figure class="img-container">
                            <img alt="single" class="" src="images/proyectos/puertas/kia_gonzalitos/kia_570x300.jpg"/>
                            <div class="post-item__description">
                            </div>
                        </figure>
                    </a>
                </div>
        </div>
    </div>
</section>
<script src="js/jquery-1.11.0.min.js" type="text/javascript">
</script>
<script src="js/jquery.nicescroll.min.js" type="text/javascript">
</script>
<script src="js/isotope.pkgd.min.js" type="text/javascript">
</script>
<script src="js/owl.carousel.min.js" type="text/javascript">
</script>
<script src="js/jquery.main.js" type="text/javascript">
</script>
<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCW5xflxz5uGqWSoplMi396PWgVJZFd5uY&callback=initMap">
</script>
<script src="js/jquery.fs.boxer.min.js" type="text/javascript">
</script>
<!-- / settings_box -->
<script src="settings_box/settings_box.js" type="text/javascript"></script>
<script src="js/vegas.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="/js/slippry.min.js"></script> -->
<script src="js/jquery.backstretch.min.js" type="text/javascript"></script>
<script>
    function initMap() {
        // Styles a map in night mode.
        //data-longitude="25.6613241" data-latitude="-100.2996392" --->
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 25.650328, lng: -100.354420},
          zoom: 14,
          styles: [{
                stylers: [{
                    saturation: -100
                }]
            }
          ]
        });
          
        var latlng = {lat: 25.650328, lng: -100.354420};

        var marker = new google.maps.Marker({
            map: map,
            icon: "images/marker2.png",
            position: latlng,
        });
    }

    $(function(){
        var nav = '<?php echo $_SERVER['REQUEST_URI'] ?>';
        nav = nav.split('/')[1];
        var a = $("a[href='"+nav+"']");
        if(a.closest('div').hasClass('submenu')){
            a.closest('div').parent().addClass('current');     
        }else{
            a.parent().addClass('current');
        }
        // $('#pictures-demo').slippry();

    });
</script>

<section class="posts-container">
    <div class="container">
        <div class="section-title">
            <h2>
                Otros torniquetes
            </h2>
        </div>
        <div id="project-slide">
            <div class="project-item post-item">
                <a href="t_optico.php">
                    <p class="post-title base-text-color">
                        Óptico
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/project_img/2_col/2.jpg"/>
                        <!-- <div class="post-item__description"> -->
                        <!-- </div> -->
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="t_ce.php">
                    <p class="post-title base-text-color">
                        Cuerpo Entero
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/project_img/2_col/6.jpg"/>
                        <!--
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="t_ce.php">
                                </a>
                            </div>
                        -->
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="torniquetes.php">
                    <p class="post-title base-text-color">
                        Tripié
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/project_img/2_col/7.jpg"/>
                        <!-- <div class="post-item__description">
                            <a class="icon-plus" data-gallery="f_project" href="torniquetes.php">
                            </a>
                        </div> -->
                    </figure>
                </a>
            </div>
            <div class="project-item post-item">
                <a href="t_od.php">
                    <p class="post-title base-text-color">
                        Óptico Deslizante
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/project_img/2_col/8.jpg"/>
                        <!-- <div class="post-item__description">
                            <a class="icon-plus" data-gallery="f_project" href="t_od.php">
                            </a>
                        </div> -->
                    </figure>
                </a>
            </div>
        </div>
    </div>
</section>
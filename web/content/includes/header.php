<section id="header-container">
    <header id="header" class="header__fixed">
        <div class="container">
            <a href="index.php" id="logo">
            </a>
            <nav class="fl-r" id="navigation" role="navigation">
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                    </li>
                    <li>
                        <a href="#">Productos</a>
                        <div class="submenu">
                            <ul class="base-bg-color">
                                <li>
                                    <a href="elevadores.php">Elevadores</a>
                                </li>
                                <li>
                                    <a href="puertas.php">Puertas automáticas</a>
                                </li>
                                <li>
                                    <a href="torniquetes.php">Torniquetes</a>
                                </li>
                                <li>
                                    <a href="brt.php">Sistema BRT</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <!-- <a href="proyectos.php">Proyectos</a> -->
                        <a href="#">Proyectos</a>
                        <div class="submenu">
                            <ul class="base-bg-color">
                                <li>
                                    <a href="p_elevadores.php">Elevadores</a>
                                </li>
                                <li>
                                    <a href="p_puertas.php">Puertas automáticas</a>
                                </li>
                                <li>
                                    <a href="p_brt.php">Sistema BRT</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="servicios.php">Servicios</a>
                       <!--  <div class="submenu">
                            <ul class="base-bg-color">
                                <li>
                                    <a href="servicios_elevadores.php">Elevadores</a>
                                </li>
                                <li> 
                                    <a href="#.php">Puertas automáticas</a>
                                </li>
                            </ul>
                        </div> -->
                    </li>
                    <li>
                        <a class="current" href="about.php">Nosotros</a>
                    </li>
                    <li>
                        <a href="contact.php">Contacto</a>
                    </li>
                    <li>
                        <a href="tel:018000028267">
                            <i class="ico icon-phone rounded_50 base-text-color base-border-color">
                            </i>
                            01 800 00 AVANS
                        </a>
                        <a href="mailto:ventas@avans.com">
                            <i class="ico icon-mail rounded_50 base-text-color base-border-color">
                            </i>
                            ventas@avans.com
                            </br>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
</section>

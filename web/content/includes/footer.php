<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4">
                <div class="footer-item">
                    <h4>
                        <b>
                            Envíanos un mensaje.
                        </b>
                        <br>
                        Queremos saber de tu proyecto.
                    </h4>
                    <p>
                    </p>
                    <form method="post">
                        <input name="nombre" placeholder="Nombre" type="text"/>
                        <input name="telefono" placeholder="Telefono" type="text"/>
                        <input name="email" placeholder="E-mail" type="text"/>
                        <!---<input type="text" placeholder="Empresa" /-->
                        <textarea name="mensaje" placeholder="Mensaje" type="text"></textarea>
                        <input class="base-text-color" type="submit" value="Enviar"/>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-8">
                <div class="contact-address">
                    <h4>
                        <br>
                        <b>
                            Nuestra ubicación
                        </b>
                    </h4>
                    <p>
                        <br>
                        <b>
                            México D.F.
                        </b>
                        <br>
                            <i class="ico icon-location rounded_50 base-text-color base-border-color">
                            </i>
                            Río Churubusco #407 Col. Unidad Modelo Del. Iztapalapa C.P. 09089 México, D.F.
                        <i class="ico icon-phone rounded_50 base-text-color base-border-color">
                        </i>
                        Tel. +52 (55) 5646 - 0046
                        <br>
                        <i class="ico icon-mail rounded_50 base-text-color base-border-color">
                        </i>
                        Email:
                        <a href="mailto:ventas@avans.com">
                            ventas@avans.com
                        </a>
                        <br>
                        <br>
                    </p>
                    <p>
                        <b>
                            Monterrey
                        </b>
                        <br>
                            <i class="ico icon-location rounded_50 base-text-color base-border-color">
                            </i>
                            León Guzmán No. 1610 Col. Nuevo Repueblo, C.P. 64700 Monterrey, N.L., México
                        <i class="ico icon-phone rounded_50 base-text-color base-border-color">
                        </i>
                        Tel. +52 (81) 1052 3738
                        <br>
                        <i class="ico icon-fax rounded_50 base-text-color base-border-color">
                        </i>
                        <i class="ico icon-mail rounded_50 base-text-color base-border-color">
                        </i>
                        Email:
                        <a href="mailto:ventas@avans.com">
                            ventas@avans.com
                        </a>
                    </p>
                    <p>
                        <br>
                        <b>
                            Interior de la República
                        </b>
                        <br>
                        <i class="ico icon-phone rounded_50 ca base-text-color base-border-color">
                        </i>
                        Tel. 01 800 00 AVANS
                        <br>
                        <i class="ico icon-mail rounded_50 base-text-color base-border-color">
                        </i>
                        Email:
                        <a href="mailto:ventas@avans.com">
                            ventas@avans.com
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12" style="text-align: center;">
                Copyright © 2019 Avance Inteligente SA de CV
            </div>
            <div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12" style="text-align: center;">
               <a href="/files/aviso_privacidad.pdf">Aviso de privacidad</a>
            </div>
        </div>
    </div>
</footer>
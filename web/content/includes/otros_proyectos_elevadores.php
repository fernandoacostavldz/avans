<section class="posts-container">
    <br>
    <div class="container">
        <div class="section-title">
            <h2>
                Otros Proyectos
            </h2>
        </div>
        <div id="project-slide">

            <div class="slide">
                <a href="torre_top.php">
                    <p class="post-title base-text-color">
                        Torre T.OP
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/top/top_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Live_Aqua.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="nest.php">
                    <p class="post-title base-text-color">
                        Torre NEST
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nest/n_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Live_Aqua.php">
                            </a> -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="nave01.php">
                    <p class="post-title base-text-color">
                        Nave 01
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/nave_01/n_570_2.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="puntacero.php">
                    <p class="post-title base-text-color">
                        PUNTACERO
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/puntacero/puntacero_edit.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="citica.php">
                    <p class="post-title base-text-color">
                        Citica
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/citica/c_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="torre_centrika.php">
                    <p class="post-title base-text-color">
                        Torre Céntrika Elite
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/centrika/ce_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="moll_valle.php">
                    <p class="post-title base-text-color">
                        Moll del Valle
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/moll_del_valle/mv_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="colegio_ingles.php">
                    <p class="post-title base-text-color">
                        Colegio inglés
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/colegio_ingles/colegioingles_edit.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="PabellonM.php">
                    <p class="post-title base-text-color">
                        Pabellón M
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/pabellon_m/pabellonm-edit.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="punto_lomas.php">
                    <p class="post-title base-text-color">
                        Punta Lomas
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/punto_lomas/pl_570.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>
            <div class="slide">
                <a href="tule.php">
                    <p class="post-title base-text-color">
                        Tulé
                    </p>
                    <figure class="img-container">
                        <img alt="single" class="img-responsive" src="images/proyectos/elevadores/tule/t_570_2.jpg"/>
                        <div class="post-item__description">
                            <!-- <a class="icon-plus" href="Equus444.php">
                            </a -->
                        </div>
                    </figure>
                </a>
            </div>

        </div>
    </div>
</section>
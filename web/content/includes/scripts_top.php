<head>
    <meta content="Accesibilidad total en los edificios que habitamos." name="description"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <!-- Favicons  -->
    <link href="images/favicon.ico" rel="shortcut icon">
    <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <!-- CSS  -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <!-- / color -->
    <link class="colors_style" href="css/color_style/color_1.css" rel="stylesheet" type="text/css"/>
    <!-- / google font -->
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,600,300" rel="stylesheet" type="text/css">
    <!-- / settings_box -->
    <link href="settings_box/settings_box.css" rel="stylesheet" type="text/css">
    <!-- Load jQuery -->
    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    <script src="js/device.min.js" type="text/javascript"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-399268-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-399268-1');
    </script>
</head>
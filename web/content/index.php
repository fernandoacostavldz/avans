<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="sp">
    <title>
        Avans - Inicio
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section class="intro-section text-center" id="intro-fullscreen">
            <div class="intro-slider intro-slider-vegas" data-dots="true" id="intro-media">
                <div class="vegas-slide vegas-transition-fade vegas-transition-fade-in vegas-transition-fade-out" style="transition: all 4000ms ease 0s;">
                    <div class="vegas-wrapper" style="overflow: hidden; padding: 0px;">
                    </div>
                </div>
                <div id="intro-content">
                    <h1>

                        Accesibilidad total en los edificios que habitamos.
                    </h1>
                </div>
            </div>
        </section>
        <section class="posts-container" style="background: #fff; margin-top: -75px;">
            <div class="container">
                <div class="section-title">
                    <h2 style="color: #3c3c3c">
                        Nuestros Productos
                    </h2>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4" style="width: 33%;">
                        <div class="project-item post-item">
                            <a href="elevadores.php">
                                <p class="post-title">
                                    Elevadores
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/2.jpg"/>
                                   <!--  <div class="post-item__description">
                                        <div class="link-container"> -->
                                            <!--<a class="icon-link" href="project_details_2.php"></a>-->
                                            <!-- <a class="icon-plus" href="elevadores.php"> -->
                                            <!-- </a> -->
                                        <!-- </div> -->
                                    <!-- </div> -->
                                </figure>
                                <p style="color: #3c3c3c">
                                    Explora nuestra variedad de elevadores: Comerciales e indsutriales.
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 hidden-sm" style="width: 33%;">
                        <div class="project-item post-item">
                            <a href="puertas.php">
                                <p class="post-title">
                                    Puertas Automáticas
                                </p>
                                <!-- </p> -->
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/3p.jpg"/>
                                    <!-- <div class="post-item__description"> -->
                                        <!-- <div class="link-container"> -->
                                            <!--<a class="icon-link" href="project_details_2.php"></a>-->
                                            <!-- <a class="icon-plus" href="puertas.php"> -->
                                            <!-- </a> -->
                                        <!-- </div> -->
                                    <!-- </div> -->
                                </figure>
                                <!-- Ulises comments -->
                                <!-- Ulises comments -->
                                <!-- Ulises comments -->
                                <p style="color: #3c3c3c">
                                    Contamos con los mejores sensores y las puertas más responsivas.
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 hidden-sm" style="width: 33%;">
                        <div class="project-item post-item">
                            <a href="torniquetes.php">
                                <p class="post-title">
                                    Torniquetes
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/2_col/2.jpg"/>
                                </figure>
                                <p style="color: #3c3c3c">
                                    Utiliza nuestros torniquetes para conocer el flujo de personas en tu negocio.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container" style="background: #fff">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 hidden-sm" style="width: 33%; margin-left: 16.5%">
                        <div class="project-item post-item">
                            <a href="pilones.php">
                                <p class="post-title">
                                    Pilones
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/4p.jpg"/>
    <!--                                 <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="pilones.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <p style="color: #3c3c3c;">
                                    Conoce nuestra variedad de pilones y sistemas de bloqueo para automóviles.
                                </p>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 hidden-sm" style="width: 33%; margin-right: 16.5%">
                        <div class="project-item post-item">
                            <a href="brt.php">
                                <p class="post-title">
                                    Sistema BRT
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/brt/brt.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="brt.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <p style="color: #3c3c3c;">
                                    Es un sistema automático de puertas para interactuar con una red de transporte público.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="infoline">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <h2 style="color: white">
                            Explora nuestros proyectos.
                        </h2>
                        <p style="color: white">
                            Nosotros proveemos soluciones de acceso, movilidad y contabilización de personal para su empresa o centro comercial. Estos son algunos de nuestros
                            <b>
                                proyectos más recientes:
                            </b>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section id="portfolio-container1">
            <div class="isotope-container" id="isotope-masonry">
                <div class="element identity" data-x="2" data-y="2">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" href="project_details_1.php" src="images/Portadas/LiveAqua.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Live_Aqua.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Grand Fiesta Americana
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element UI" data-x="1" data-y="1">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas/FashionDrive.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Fashion_Drive.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Fashion Drive
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element mobile" data-x="1" data-y="2">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img class="img-responsive" src="images/Portadas/Arboleda.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Arboleda.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Arboleda
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element interactive" data-x="1" data-y="1">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img class="img-responsive" src="images/Portadas/Restaurante.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Restaurante.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Restaurante
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element creative UI" data-x="2" data-y="2">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img class="img-responsive" src="images/Portadas/Equus335.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Equus335.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Equus 335
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element identity" data-x="2" data-y="1">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas/HolyCow.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Holy_Cow.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Holy Cow
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element interactive" data-x="1" data-y="1">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas/Equus444.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Equus444.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Equus 444
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element creative" data-x="1" data-y="1">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img class="img-responsive" src="images/Portadas/TrebolPark.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="Trebol_Park.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Trebol Park
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
                <div class="element UI" data-x="3" data-y="1" style=" top: 907px;">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas/Pabellon M.jpg"/>
                            <div class="post-item__description">
                                <div class="link-container">
                                    <a class="icon-plus" href="PabellonM.php">
                                    </a>
                                </div>
                                <h1 class="item-title">
                                    Pabellon M
                                </h1>
                                <p class="item-p">
                                    <br>
                                        <br>
                                            <br>
                                                Elevadores / Puertas Autómaticas
                                            </br>
                                        </br>
                                    </br>
                                </p>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
        <section class="services services-with-border_b">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-ok rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Somos eficientes
                                </h3>
                            </div>
                            <p>
                                Diseñamos la solución perfecta para el espacio y necesidades que tenga tu negocio o comercio y llevamos a cabo la instalación más rápida.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-mobile rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Sistemas de monitoreo
                                </h3>
                            </div>
                            <p>
                                Entendemos la complejidad que implica revisar los sitemas de acceso las 24 horas del día. Nuestros sistemas pueden monitorearse en tiempo real a distancia.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-paper-plane rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Materiales de alta seguridad
                                </h3>
                            </div>
                            <p>
                                Nuestra experiencia respalda la calidad que manejamos en cada uno de nuestros proyectos. Tomamos la seguridad como una de nuestras prioridades.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
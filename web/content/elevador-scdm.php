<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Elevador Comercial sin Cuarto de Maquina
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Elevador Comercial sin Cuarto de Máquina
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/banner-sin-cuarto.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/banner-sin-cuarto.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="project-content">
                            <h4 class="project-title">
                                Elevadores que optimizan el espacio al no requerir cuarto de máquinas y desplazarse en un cubo mas reducido.
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características
                            </h2>
                            <h4>
                                - Equipos que ahorran hasta el 10% del área de construcción del cubo del elevador.
                                <br/>
                                - La transmisión directa del motor ofrece lo siguiente: Reducción considerable del consumo de energía, menor desgaste de partes y una operación más silenciosa.
                                <br/>
                                - Los motores requieren mínima lubricación lo cual se traduce en una reducción del costo de mantenimiento.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Aplicaciones
                    </h2>
                </div>
                <div class="logos-carousel1 logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/edificios_corporativos.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/edificios_comerciales.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/edificios_departamentales.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/hospitales.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/centros_comerciales.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/escuelas_universidades.png"/>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
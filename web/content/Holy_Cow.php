<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Holy Cow
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Holy Cow
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar" >
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Glass
                                </li>
                                <li>
                                   Montura: Dentro del Marco / O-X-X-O
                                <li>
                                   Vidrio: Templado / Claro / 9mm
                                </li>
                                <li>
                                    Acabado: Anodizado Natural Mate
                                </li>
                                <li>
                                    Perfil: Europeo
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Mayor agilidad en la entrega de alimentos
                            </h2>
                            <p style="text-align: justify;">
                               La cadena Holy Cow, Burger & Beer tiene tres sucursales en el área metropolitana una de ella está en Fashion Drive, ubicado en San Pedro.
                               <br>
                               Esta sucursal cuenta con un área interior y otra exterior separadas por un pasillo. En este proyecto fue necesario instalar accesos que permitirán disfrutar a los comensales de los beneficios del exterior, pero que estuvieran protegidos de las inclemencias del clima, y que los meseros pudieran transportar los alimentos y bebidas de forma ágil y sin complicaciones.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"/images/proyectos/puertas/holy_cow/holy_cow3.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.2,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
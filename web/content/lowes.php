<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Lowes
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Lowes
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>Tipo de Puerta: SO-SX-SX-SO </li>
                                <li>Montura: Dentro del marco </li>
                                <li>Vidrio: Vidrio templado claro de 6 mm de espesor </li>
                                <li>Perfil aluminio: Mediano - zoclo inferior de 10” y barra intermedia de 4” </li>
                                <li>Acabado aluminio: Natural anodizado claro </li>
                                <li>Cerrojo: Cerrojo manual (mariposa por dentro, llave por fuera) </li>
                                <li>Activación y seguridad: 2 x Sensor de movimiento y presencia modelo IXIO </li>
                                <li>Sistema de Emergencia: Abatimiento de sus 4 hojas hacia el exterior </li>
                                <li>Control: Control manual de 5 funciones (abierto/cerrado/automático/parcial/solo salida) </li>
                                <li>Accesorios: Switch magnético indicador de posición de la puerta y Monitoreo de abatimiento de hojas (para alarma)</li>

                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Puertas de altura
                            </h2>
                            <p style="text-align: justify;">
                               Una de las características más importantes de las puertas de Lowes es la altura (3mts), pues es muy común que los consumidores adquieran artículos como tarimas, muebles y herramientas, que son muy voluminosos a lo largo y ancho.
                               Se instalaron puertas automáticas deslizantes de cuatro hojas con marco de aluminio mediano, con barras de seguridad y sensores de presencia en ambos lados de la puerta, con el objetivo soportar los golpes que los clientes y/o empleados le pudieran dar a las hojas de la puerta debido a las maniobras que ahí se realizan.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
        <script type="text/javascript">
            $(function(){
                $("#backstretch-photo").backstretch([
                    {
                        // url:"/images/proyectos/pabellonm/pabellonm.jpg?width={width}&height={height}",
                        url:"/images/proyectos/puertas/lowes/L_1000.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.3,
                    },
                    {
                        url:"/images/proyectos/puertas/lowes/L2_1000.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.1
                    },                     
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
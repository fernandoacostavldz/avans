<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Nosotros
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Únete a nuestro equipo
                        </b>
                    </h2>
                    
                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        <li>Empresa</li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="teams base-bg-color_light">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="section-title">
                        <br>
                        <p>
                            Somos un equipo joven con espíritu innovador y actitud de servicio. Cada una de las personas que integramos Avance Inteligente somos parte fundamental para su operación y crecimiento.
                        </p>
                        <p>
                            Ofrecemos un entorno de trabajo motivante y retador. Fomentamos la aportación de ideas de cada miembro de nuestro equipo para mejorar nuestros productos, actividades y relaciones con nuestros clientes.
                        </p>
                        <p>
                            Si te identificas con nuestros valores y te interesa crecer con nosotros, te invitamos a que nos escribas a rh@avans.com incluyendo tu curriculum y una breve descripción de qué sientes que puedes aportar a nuestro equipo.
                        </p>
                        <!-- <a class="more-btn" href="#"><i class="icon-angle-right rounded_50 base-border-color"></i> Discover More</a> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <div class="title-with-ico" style=" margin-top: 10px;">
                            <i class="ico icon-users rounded_50 base-text-color base-border-color">
                            </i>
                            <h3>
                                <b>
                                    Nuestro Equipo
                                </b>
                            </h3>
                        </div>
                        <p>
                            Creemos en el trabajo en equipo y la colaboración con otras empresas.
                        </p>
                        <p>
                            Tenemos desarrollados distintos planes de asociación para empresas interesadas en distribuir nuestros equipos.
                        </p>
                    </div>
                </div>
                <div class="row" id="teams-wrapper">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="team-item" style="min-height:0px; ">
                            <header>
                                <figure>
                                    <img alt="single" class="img-responsive" src="images/team_img/3_Col/1.jpg"/>
                                </figure>
                                <!--<div class="social">
                                    <a class="icon-dribbble" href="#"></a>
                                    <a class="icon-linkedin-rect" href="#"></a>
                                    <a class="icon-facebook-rect" href="#"></a>
                                </div>-->
                            </header>
                            <h3>
                                Espíritu Innovador
                            </h3>
                            <span>
                            </span>
                            <!-- <p>
                                It's unlimited choice flip cool clean amazing shine. Selfish spend on your unique value quick inside amazing splash distinct happy. Very brighter to action bigger the colossal goodbye yet sparkling think exotic. Value chocolatey pleasing spend bigger however discount.
                            </p> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="team-item" style="min-height:0px; ">
                            <header>
                                <figure>
                                    <img alt="single" class="img-responsive" src="images/team_img/3_Col/2.jpg"/>
                                </figure>
                            </header>
                            <h3>
                                Entorno Motivante
                            </h3>
                            <span>
                            </span>
                       <!--      <p>
                                First rated artificial leading cheap go roasted comfort clinically velvety deluxe secure. Coming warm blast so healthy. mountain bonus reduced lather stains buy believe hit shine trust. Flexible however why below moist talking special confident exotic. Sporty announcing we magically terrific what dual this takes.
                            </p> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="team-item" style="min-height:0px; ">
                            <header>
                                <figure>
                                    <img alt="single" class="img-responsive" src="images/team_img/3_Col/3.jpg"/>
                                </figure>
                            </header>
                            <h3>
                                Crecimiento Profesional
                            </h3>
                          <!--   <span>
                            </span>
                            <p>
                                Prevents desire brings intense. Good mothers rosy. Effective simulated not calories care find the brighter chunky newest compact wave.
                            </p>
                            <p>
                                Fights distinct buttery choice commercial spend on disposable think energy flexible.
                            </p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
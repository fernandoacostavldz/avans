<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Marco de aluminio
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta deslizante:
                        </b>
                        Marco de aluminio
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/DEZLIZANTES/marco-de-aluminio/puertas-automaticas-marco-de-aluminio-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        El modelo más popular de puerta deslizante en Europa y el mundo está ahora en México. La puerta Unislide es ideal para lugares de alto flujo donde el perímetro de las hojas pueda estar expuesto a golpes de los usuarios.
                                    </p>
                                    <p>
                                        Este modelo presenta un marco de aluminio redondeado en perfecta armonía con el cabezal BESAM. Su sistema de emergencia permite abatir sus cuartro hojas hacia el exterior en caso de pánico. El sistema de sensores Ultraview, exclusivo de BESAM, es el más seguro, moderno y estilizado del mercado.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        Montura superficial
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-sup/PA_deslizante_marcoaluminio_superficial_2.7.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-sup/AC_PA_deslizante_marcoaluminio_superficial_2.7.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla SA P-SX (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-sup/PA_deslizante_marcoaluminio_superficial_2.10.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble SA P-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-sup/AC_PA_deslizante_marcoaluminio_superficial_2.10.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble SA P-SX (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                    <p>
                                        Montura dentro del marco
                                    </p>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-d-marco/PA_deslizante_marcoaluminio_dentromarco_2.1.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC SO-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-d-marco/AC_PA_deslizante_marcoaluminio_dentromarco_2.1.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Sencilla OHC SO-SX (AutoCad)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-d-marco/PA_deslizante_marcoaluminio_dentromarco_2.4.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC SO-SX (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-deslizantes/marco-aluminio/mon-d-marco/AC_PA_deslizante_marcoaluminio_dentromarco_2.4.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Doble OHC SO-SX (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otras_puertas.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
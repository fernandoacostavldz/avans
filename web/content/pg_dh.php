<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Dos Hojas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta giratoria:
                        </b>
                        Dos Hojas
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/puertas-automaticas-dos-hojas-1.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/puertas-automaticas-dos-hojas-1.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Nuestro modelo Two Wing es imponente por naturaleza. Su gran dimensión permite incrementar el flujo de personas de forma natural protegiendo el edificio de la transferencia de calor a través del flujo de aire – lo que trae consigo ahorros importantes en materia de costos energéticos.
                                    </p>
                                    <p>
                                        Sus hojas centrales se abaten hacia el exterior en caso de pánico y su sistema de sensores maximiza la seguridad para el usuario de este equipo.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-giratorias/dos-hojas/PA_giratoria_doshojas_dentromarco_1.1.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Dos Hojas (PDF)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-dos-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_dh.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_dh.php">
                            <p class="post-title base-text-color">
                                Dos Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-tres-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_th.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_th.php">
                            <p class="post-title base-text-color">
                                Tres Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-cuatro-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_ch.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_ch.php">
                            <p class="post-title base-text-color">
                                Cuatro Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-manuales.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Manuales
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-alta-seguridad1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_as.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_as.php">
                            <p class="post-title base-text-color">
                                Alta Seguridad
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>        
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Elevador Industrial Capacidad Menor
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Elevador Industial Capacidad Menor
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/banner-elevadores-industriales.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/caracteristicas-capacidad-menor.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="project-content">
                            <h4 class="project-title">
                                Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características
                            </h2>
                            <h4>
                                - Diferentes opciones en puertas de acceso.
                                <br/>
                                - Habilitación de cabina para uso rudo (opcional).
                                <br/>
                                - Diferentes acabados para ajustarse a las necesidades del cliente.
                                <br/>
                                - Tamaños de cabina según capacidad de carga.
                                <br/>
                                - Sujetos a códigos americanos de operación.
                                <br/>
                                - Manufactura de equipos en USA.
                                <br/>
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características Opcionales
                            </h2>
                            <h4 class="project-title">
                                - Velocidad:
                            </h4>
                            <h4>
                                0.25mps a 0.75mps
                            </h4>
                            <h4 class="project-title">
                                - Capacidad:
                            </h4>
                            <h4>
                                1000 kg a 3000kg
                            </h4>
                            <h4 class="project-title">
                                - Recorrido de hasta:
                            </h4>
                            <h4>
                                2.5 m
                            </h4>
                            <h4 class="project-title">
                                - Equipo no presionado
                            </h4>
                            <h4 class="project-title">
                                - Fase I y II contra incendios
                            </h4>
                            <h4 class="project-title">
                                - Norma N-18
                            </h4>
                            <h4 class="project-title">
                                - Puertas contra fuego
                            </h4>
                            <h4 class="project-title">
                                - Detección y monitoreo de fallas
                            </h4>
                            <h4 class="project-title">
                                - Voltaje y frecuencia variable
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Aplicaciones
                    </h2>
                </div>
                <div class="logos-carousel1 logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/supermercados.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/almacenes.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/fabricas.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/talleres.png"/>
                    </div>
                </div>
            </div>
        </section>
        <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Sistemas de carga
                    </h2>
                </div>
                <div class="logos-carousel1 logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/traccion_de_cadena.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/sistema_hidraulico.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/traccion_de_cable.png"/>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
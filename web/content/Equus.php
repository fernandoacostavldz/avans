<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Equus
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Equus 444 y 335
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                  Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Glass LT
                                </li>
                                <li>
                                    Montura: Superficial / O-X-X-O
                                </li>
                                <li>
                                    Vidrio: Templado / Claro / 9mm
                                </li>
                                <li>
                                    Acabado: Anodizado Natural Mate
                                </li>
                                <li>
                                    Perfil: Europeo
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Seguridad en edificios
                            </h2>
                            <div style="text-align: justify;">
                                En ambos proyectos la desarrolladora confío en Avans para la instalación de puertas y torniquetes, siendo uno de los objetivos principales la seguridad de las personas.
                                <br>
                                Cada de las puertas automáticas giratorias cuentan con un bumper a nivel de talón que para la puerta en caso de la hoja golpee a una persona u objeto. Las hojas cuentan con cerdas perimetrales que evitan el paso del aire y previenen la perdida de energía, el techo de cristal de una transparencia única a estos equipos.
                                <br>
                                Se selecciono una puerta de tres hojas para poder ampliar el espacio interior de la puerta y cuatro hojas para poder incrementar el flujo de personas.
                                <br>
                                Para lograr una total transparencia en las puertas giratorias se decidió colocar los operadores por debajo de nivel de piso terminado. El motor la tarjeta y otros componentes electrónicos se colocan de forma subterránea por lo cual son invisibles a la percepción humana, 
                                <br>
                                Este sistema está diseñado para que el mantenimiento sea fácil de hacer.
                            </div>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"/images/proyectos/puertas/equus/e2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.9,
                },                    
                {
                    url:"/images/proyectos/puertas/equus/e6_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },   
                {
                    url:"/images/proyectos/puertas/equus/e3_1400.jpg",  
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.8,
                },
                {
                    url:"/images/proyectos/puertas/equus/e4_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                },
                {
                    url:"/images/proyectos/puertas/equus/e5_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.9,
                },
                {
                    url:"/images/proyectos/puertas/equus/e1_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.8,
                },   
                {
                    url:"/images/proyectos/puertas/equus/e7_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },   
              ],{
                duration:1000,
              }
            );
        });
    </script>
</html>
<!DOCTYPE html>
<html>
	<head>
	<title>Avans | Sistema BRT</title>
	<?php include 'includes/scripts_top.php'; ?>
	<style type="text/css">
		.tour .cont {
		    width: 960px;
		    margin: auto;
		    color: #fff
		}
		.tour .cont p {
		    font-size: 18px
		}

		.tour .cont ul {
		    text-align: left;
		    padding-top: 50px
		}

		.tour .cont ul li {
		    display: inline-block;
		    width: 250px;
		    margin: 0 50px
		}

		.tour .cont ul li a {
		    display: block;
		    outline: none;
		    text-decoration: none;
		    color: #525452;
		    /*text-transform: uppercase;*/
		    /*font-size: 18px;*/
		    position: relative;
		    text-align: center;
		}

		.tour .cont ul li a p {
		    padding-top: 20px
		}

		.tour .cont ul li a img {
		    border-radius: 50%;
		    -webkit-transition: all 0.3s;
		    -moz-transition: all 0.3s;
		    -o-transition: all 0.3s;
		    transition: all 0.3s;
		    position: relative;
		    z-index: 2
		}

		.tour .cont ul li a:before {
		    content: '';
		    display: block;
		    width: 65px;
		    height: 65px;
		    background: url(brt/images/lupa.png);
		    position: absolute;
		    top: 0px;
		    left: 50%;
		    margin-left: -32px;
		    z-index: 3;
		    opacity: 0;
		    -webkit-transition: all 0.3s;
		    -moz-transition: all 0.3s;
		    -o-transition: all 0.3s;
		    transition: all 0.3s
		}

		.tour .cont ul li a:after {
		    content: '';
		    display: block;
		    position: absolute;
		    top: 0;
		    left: 0;
		    width: 250px;
		    height: 250px;
		    box-shadow: inset 0px 0px 0px 6px #a8e764;
		    border-radius: 50%;
		    z-index: 1;
		    opacity: 0;
		    -webkit-transition: all 0.3s;
		    -moz-transition: all 0.3s;
		    -o-transition: all 0.3s;
		    transition: all 0.3s
		}

		.tour .cont ul li a:hover:before {
		    opacity: 1;
		    top: 90px
		}

		.tour .cont ul li a:hover:after {
		    opacity: 1;
		}

		.tour .cont ul li a:hover img {
		    -webkit-transform: scale(0.8);
		    -moz-transform: scale(0.8);
		    -o-transform: scale(0.8);
		    transform: scale(0.8)
		}
	</style>
	<body>
		<?php include 'includes/header.php'; ?>
		<!-- SLIDE SHOW -->
		    <div id="backstretch-photo">
		    </div>
		<!-- / SLIDE SHOW -->
		<section class="title-bottom-picture">
		    <h2 class="fl-l">  
		        Sistema BRT
		    </h2>
		</section>
		<section id="content-container" style="padding-top:0px;">
		    <div class="container">
		        <div class="row">
		            <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
		                    <h2 class="project-title" style=" font-weight: 300;">
		                        Descripción del Proyecto
		                    </h2>
		                    <p style="text-align: justify;">
								Se cuenta con estaciones de acceso para los usuarios y por lo general se utilizan estaciones terminales en los dos extremos de la línea de transporte.
								Las puertas automáticas permiten generar zonas de comfort al aislar a los usuarios del ruido, contaminación y clima adverso. así mismo, incrementan la seguridad de las estaciones aunado a la imagen de modernidad que representan. Nuestras puertas están especialmente diseñadas para operar en condiciones de uso extremo como lo es el transporte público en zonas urbanas.
		                    </p>
		                </div>
		            </div>
				</div>
			</div>
		</section>
		<div class="container">
			<div class="section-title clearfix">
				<h2>Tour virtual</h2>
			</div>
		</div>
		<div class="tour">
			<div class="cont">
				<ul class="clearfix">
					<li>
						<a href="brt/ecovia/ecovia.html" target="_blank">
							<img src="/brt/images/5tourestacion.jpg" height="250" width="250">
							<p>Estación</p>
						</a>
					</li>
					<li>
						<a href="brt/ecovia/terminal.html" target="_blank">
							<img src="/brt/images/5tourbtnterminal.jpg" height="250" width="250">
							<p>Terminal</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<br>
		<br>
		<?php include 'includes/otros_proyectos.php'; ?>
		<?php include 'includes/footer.php'; ?>
		<?php include 'includes/scripts_bottom.php'; ?>
	</body>
	<script type="text/javascript">
	    $(function(){
	        $("#backstretch-photo").backstretch([
	            {
	                url:"images/brt/brtt.jpg",
	                transition:'fade',
	                scale:'cover',
	                fade:1000,
	                alignY:.6,
	            },
	          ],{
	            duration:3500,
	          }
	        );
	    });
	</script>
</html>
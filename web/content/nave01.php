<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre top
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Nave 01
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Edificio de usos mixtos
                            </h2>
                            <p style="text-align: justify;">
                                Para este proyecto Avans elaboró los estudios de tráfico y la combinación necesaria para determinar el número de elevadores y escaleras eléctricas, indispensables en los edificios para que el tráfico vertical tanto en oficinas como plaza comercial y departamentos de forma eficiente.
                                <br>
                                Además, cuenta con escaleras eléctricas para la transportación vertical de un tráfico intenso.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50

                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 16</li>
                                        <li>Velocidad: 1.00 a 1.75 m/s</li>
                                        <li>Capacidad: 1200 a 3000 kg</li>
                                        <li>Paradas: 5 a 13</li>
                                        <li>Tipo: Con y sin cuarto de máquina</li>
                                        <li>Diseño: Pasajeros y montacargas</li>
                                        <li>Altura de puertas: 2.30 mts</li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Escaleras
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 8</li>
                                        <li>Intemperie: si</li> 
                                        <li>2 Escalones de desembarque</li>
                                        <li>Con economizador de energía</li>
                                        <li>Alturas de 3.50 mts, 4.20 mts y 6.00mts</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/nave_01/n4_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },
                {
                    url:"images/proyectos/elevadores/nave_01/n3_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.5,
                },
                // {
                //     url:"images/proyectos/elevadores/nave_01/N 1400.jpg",
                //     transition:'fade',
                //     scale:'cover',
                //     fade:1000,
                //     alignY:.8,
                // },
                {
                    url:"images/proyectos/elevadores/nave_01/n2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.5,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Puertas Autómaticas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Galería de Proyectos
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Proyectos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Live_Aqua.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Grand Fiesta Americana
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Elevadores recomendados en edificios donde es factible colocar un cuarto de máquinas en la azotea del mismo.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Fashion_Drive.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Fashion Drive
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Elevadores que optimizan el espacio al no requerir cuarto de máquinas y desplazarse en un cubo más reducido.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Arboleda.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Arboleda
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/Restaurante.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Restaurante.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Restaurante
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Uso en aplicaciones industrial y doméstico para el transporte de material entre diferentes niveles.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 idnetity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/Equus335.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Equus335.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Equus 335
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Para todo tipo edificio con alto volumen de tráfico entre pisos.
                                        <br/>
                                        <br/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/HolyCow.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Holy_cow.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Holy Cow
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Equus444.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Equus 444
                                <br/>
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="Trebol_Park.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Trebol Park
                                <br/>
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" height="200px" src="images/Portadas1/PabellonM.jpg" width="270px"/>
                                <div class="post-item__description">
                                    <div class="link-container">
                                        <a class="icon-plus" href="PabellonM.php" target="_blank">
                                        </a>
                                    </div>
                                </div>
                            </figure>
                            <p class="post-title base-text-color">
                                Pabellon M
                                <br/>
                            </p>
                            <ul class="tags-list">
                                <li>
                                    <a href="#">
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
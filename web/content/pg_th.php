<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <title>Avans - Tres Hojas</title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l"><b>Puerta giratoria:</b> Tres Hojas</h2>

                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>Productos</li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div id="project-slider_big" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 770x600/GIRATORIAS/puertas-automaticas-tres-hojas-1.jpg" alt="single" /></div>
                            </div>

                            <div id="project-slider_small" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 170x140/GIRATORIAS/puertas-automaticas-tres-hojas-1.jpg" alt="single" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside id="sidebar" class="project-info">
                            <h2>Características</h2>
                            <ul>
                                <li> 
                                    <p>
                                        Nuestra puerta giratoria de tres hojas permite al usuario transitar con maletas e inclusive sillas de ruedas. Está diseñada para cumplir con todos los estándares de seguridad y calidad internacionales.
                                    </p>

                                    <p>
                                        SSus tres hojas se abaten al centro en caso de pánico o emergencia, cuenta con un botón de seguridad y protección electrónica en los costados de las hojas curvas fijas, así como sensores de seguridad el eje horizontal de cada hoja.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">Archivos descargables</p>
                                    <br/>
                                    <p> 
                                        <a class="more-btn" href="pdf/puertas/puertas-giratorias/tres-hojas/PA_giratoria_treshojas_dentromarco_1.2.pdf"><i class="icon-angle-right rounded_50 base-border-color"></i> Tres Hojas (PDF)</a><br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-giratorias/tres-hojas/PA_giratoria_treshojas_dentromarco_1.2.dwg"><i class="icon-angle-right rounded_50 base-border-color"></i> Tres Hojas (AutoCad)</a><br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>

                    
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-dos-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_dh.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_dh.php">
                            <p class="post-title base-text-color">
                                Dos Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-tres-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_th.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_th.php">
                            <p class="post-title base-text-color">
                                Tres Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-cuatro-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_ch.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_ch.php">
                            <p class="post-title base-text-color">
                                Cuatro Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-manuales.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Manuales
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-alta-seguridad1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_as.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_as.php">
                            <p class="post-title base-text-color">
                                Alta Seguridad
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
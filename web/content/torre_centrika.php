<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Centrika Elite
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Centrika Elite
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Ingeniería de movilidad
                            </h2>
                            <p style="text-align: justify;">
                                Complejo habitacional integrado por dos torres de departamentos, que ofrecen comodidad y elegancia para todos sus habitantes.
                                <br>
                                En este proyecto se instaló un elevador de servicio exclusivo para las mudanzas de sus habitantes, de esta forma cualquier habitante del edificio puede transportar sus muebles sin importar el tamaño, pues el elevador de servicio tiene una capacidad de 1000 kg.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 3</li>
                                        <li>Velocidad: 1 m/s</li>
                                        <li>Capacidad: 800 a 1000 kg</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puerta: 2.10 mts</li>
                                        <li>Paradas: 10</li>
                                        <li>Tipo:  Sin cuarto de montacargas</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/centrika/centrika2.png",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.4,
                },
                {
                    url:"images/proyectos/elevadores/centrika/ce2_1400.jpg", 
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.4,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
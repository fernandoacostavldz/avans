<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en"> <!--<![endif]-->
    <title>Avans - Contacto</title>
    <head>
        <?php include 'includes/scripts_top.php'; ?>
    </head>

    <body>
        <?php include 'includes/header.php'; ?>

        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l"><b>Conoce Nuestra Empresa</b></h2>

                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        
                        <li>Empresa</li>
                    </ul>
                </div>
            </div>
        </section>


        <section class="map" data-longitude="25.6611431" data-latitude="-100.2997221" data-marker="images/marker.png"></section>

        <section id="headline" style="color: transparent;">
            <div class="container">
                <div class="section-title clearfix">
                    <a href="https://www.google.com/maps/place/AVANS/@25.6610897,-100.3019051,17z/data=!3m1!4b1!4m5!3m4!1s0x8662bfcc2b8c184b:0x8b858076aa013878!8m2!3d25.6610897!4d-100.2997164">
                        <h2 class="fl-l" >Oficina Monterrey</h2>
                    </a>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
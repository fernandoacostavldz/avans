<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Live Aqua
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Grand Fiesta Americana
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Ventajas
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Los huéspedes del hotel entran y salen libremente aún con equipaje
                                </li>
                                <li>
                                    Modernidad
                                </li>
                                <li>
                                    Ahorro de energía
                                </li>
                                <li>
                                    El aspecto transparente de la puerta va de la mano con el edificio
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Ahorro de energía
                            </h2>
                            <p style="text-align: justify;">
                                Innovador hotel con una hospitalidad excepcional ubicado en la zona más lujosa de Monterrey. 
                                <br>
                                Parte de sus detalles especiales están en sus puertas automáticas, las cuales ayudan a disminuir la entrada de aire caliente, lo que representa un ahorro de energía.
                                <br>
                                Para este proyecto Avans utilizó puertas de cristal, modelo Glass LT, montadas de manera superficial con componentes y perfiles de aluminio europeos.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/Live_Aqua/wide5.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                    alignX:.60,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
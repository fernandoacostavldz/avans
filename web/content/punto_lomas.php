<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Punto Lomas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Punto Lomas
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                A la medida en dimensiones y estructura
                            </h2>
                            <p style="text-align: justify;">
                                Los giros comerciales de esta plaza incluyen oficinas, tiendas para niños, salones de belleza, consultorios médicos, dentales y terapéuticos, por eso era necesaria el diseño y la instalación de elevadores panorámicos.
                                <br>
                                La plaza cuenta con 38 locales distribuidos en 3 pisos, además tiene 7 pisos de estacionamiento subterráneo. 
                                Para ser más agradable la visita de los consumidores se eligió una estructura metálica cilíndrica con vista panorámica. El análisis del proyecto y la implementación de los elevadores comprendió un periodo de 24 meses.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <i>Cantidad: 2</i>
                                        <i>Velocidad: 1.75 m/s</i>
                                        <i>Capacidad: 1000 kg</i>
                                        <i>Paradas: 10</i>
                                        <i>Tipo: Sin cuarto de máquinas </i>
                                        <i>Diseño: Semipanorámicos</i>
                                        <i>Puertas: 2.10 mts</i>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/punto_lomas/pl_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.3,
                },
                {
                    url:"images/proyectos/elevadores/punto_lomas/pl2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.3,
                },
              ],{
                duration:1000,
              }
            );
        });
    </script>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> 
    <html class="no-js" lang="en"> <!--<![endif]-->
    <title>Avans - Transporte Público</title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l"><b>Puerta deslizante:</b> Transporte Público</h2>

                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>Productos</li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    

                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div id="project-slider_big" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/transporte-publico/transporte1.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/transporte-publico/transporte2.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/transporte-publico/transporte3.jpg" alt="single" /></div>
                            </div>

                            <div id="project-slider_small" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/transporte-publico/transporte1.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/transporte-publico/transporte2.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/transporte-publico/transporte3.jpg" alt="single" /></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <aside id="sidebar" class="project-info">
                            <h2>Características</h2>
                            <ul>
                                <li> 
                                    <p>
                                        La puerta CGL LT es nuestro modelo minimalista en su diseño. Diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia. Los vidrios templados con cantos pulidos y abrillantados de forma perimetral están unidos a hueso contra el piso y costados, lo que da una sensación de transparencia y modernidad incomparable. 
                                    </p>

                                    <p>
                                        La puerta CGL LT cuenta con una batería de respaldo que acciona la puerta en caso de falta de energía y un cerrojo electrónico no visible totalmente en línea con la estética limpia del sistema.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-marco-aluminio.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_marcoaluminio.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_marcoaluminio.php">
                            <p class="post-title base-text-color">
                                Marco de Aluminio
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-totalmente-cristal.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_totalmentecristal.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_totalmentecristal.php">
                            <p class="post-title base-text-color">
                                Totalmente de Cristal
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-zoclo-inferior.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_zocloinferior.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_zocloinferior.php">
                            <p class="post-title base-text-color">
                                Zoclo Inferior
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-telescopica.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_telescopica.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_telescopica.php">
                            <p class="post-title base-text-color">
                                Telescópica
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-macropuertas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_macropuertas.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_macropuertas.php">
                            <p class="post-title base-text-color">
                                Macro Puertas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-cuarto-limpio.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_cuartolimpio.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_cuartolimpio">
                            <p class="post-title base-text-color">
                                Cuarto Limpio
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-artisticas1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_art.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_art.php">
                            <p class="post-title base-text-color">
                                Artísticas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-manuales-ICU-CCU1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_manuales.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_manuales.php">
                            <p class="post-title base-text-color">
                                Manuales ICU CCU
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-transporte-publico1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pd_tp.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pd_tp.php">
                            <p class="post-title base-text-color">
                                Transporte Público
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
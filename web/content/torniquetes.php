<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Torniquetes
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        Torniquetes
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="t_optico.php">
                                <p class="post-title base-text-color">
                                    <b>Óptico</b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/2_col/2.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="t_optico.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </div>
                                <ul class="tags-list">
                                    <p style="color: #3c3c3c">
                                        Este torniquete permite el control discreto de su personal, al contar con una alarma visual y/o auditiva que se activa al no pasar la identificación aceptada por el sistema de control de accesos.
                                    </p> 
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 UI">
                        <div class="project-item post-item">
                            <a href="t_ce.php">
                                <p class="post-title base-text-color">
                                    <b>Cuerpo Entero</b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/2_col/6.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="t_ce.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </div>
                                <ul class="tags-list">
                                    <p style="color: #3c3c3c">
                                        Torniquetes de máxima seguridad y aplomo, el modelo Full Height está diseñado para proveer años de protección en su entrada sin mantenimiento alguno. Este torniquete incluye entradas para cualquier tipo de control de accesos electrónico.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                <br>
                                <b>Tripié</b>
                            </p>
                            <div class="img-container">
                                <img alt="single" class="img-responsive" src="images/project_img/2_col/7.jpg"/>
                            </div>
                            <ul class="tags-list">
                                <p style="color: #3c3c3c">
                                    Diseñados para proteger los activos de su empresa, los torniquetes de tripié son la base de todo control de flujo de personas. Nuestro modelo integra componentes de acero inoxidable, una estética moderna y la facilidad de integrar cualquier sistema de control de accesos a nuestro mecanismo.
                                </p>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 interactive">
                        <div class="project-item post-item">
                            <a href="t_od.php">
                                <p class="post-title base-text-color">
                                    <b>Ópticos Deslizantes</b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/2_col/8.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="t_od.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </div>
                                <ul class="tags-list">
                                    <p style="color: #3c3c3c">
                                        Este torniquete permite el control discreto de su personal, al contar con una alarma visual y/o auditiva que se activa al no pasar la identificación aceptada por el sistema de control de accesos. Este equipo es comúnmente utilizado en lugares de alto flujo donde se tenga un guardia de seguridad que apoyaría en las labores de revisión.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts_bottom.php'; ?>
    <?php include "chat.php"; ?>
</html>
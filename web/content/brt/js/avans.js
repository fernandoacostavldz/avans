$(document).ready(function(){
	$('#carousel-gallery').touchCarousel({				
		itemsPerPage: 1,				
		scrollbar: true,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",				
		pagingNav: false,
		snapToItems: false,
		scrollToLast: true,
		useWebkit3d: true,				
		loopItems: true
	});

	$('.sistema article, .soluciones span').transition({ x:'-40px', opacity: 0 },0);
	$('.sistema img, .soluciones p').transition({ x:'40px', opacity: 0 },0);
	
	$(".sistema article").on("inview",function(){
		$(this).delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack');
		$('.sistema img').delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack');
	});

	$(".soluciones").on("inview",function(){
		$('.soluciones span').delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack');
		$('.soluciones p').delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack');
	});

	$('.caract .tit').transition({ x:'-40px', opacity: 0 },0);
	$('.caract .globe, .caract .puertas, .caract .stop, .caract .torniquetes').transition({ y:'-40px', opacity: 0 },0);
	$(".caract").on("inview",function(){
		$('.caract .tit').delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack',function(){
			$('.caract .puertas, .caract .torniquetes').delay(200).transition({ y:'0px', opacity: 1 },700,'easeOutExpo',function(){
				$('.caract .puertas .izq').addClass('aniPuertaIzq');
				$('.caract .puertas .der').addClass('aniPuertaDer');
				$('.caract .b1').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
					$('.caract .b5').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
						$('.caract .b2').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
							$('.caract .b3').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
								$('.caract .b4').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
									$('.caract .b6').transition({ y:'0px', opacity: 1 },300,'easeOutBack');
								});
							});
						});
					});
				});
			});
		});
	});

	$('.andenes .tit, .andenes .disc').transition({ x:'40px', opacity: 0 },0);
	$('.andenes .alerta').transition({ x:'-40px', opacity: 0 },0);
	$('.andenes .puerta').transition({ y:'40px', opacity: 0 },0);
	$('.andenes .globe').transition({ y:'-40px', opacity: 0 },0);
	$(".andenes").on("inview",function(){
		$('.andenes .tit').delay(300).transition({ x:'0px', opacity: 1 },500,'easeOutBack',function(){
			$('.andenes .puerta').delay(200).transition({ y:'0px', opacity: 1 },700,'easeOutExpo',function(){
				$('.andenes .puerta .izq1').addClass('aniPuerta1');
				$('.andenes .puerta .izq2').addClass('aniPuerta2');
				$('.andenes .puerta .der1').addClass('aniPuerta4');
				$('.andenes .puerta .der2').addClass('aniPuerta3');
				$('.andenes .alerta').transition({ x:'0px', opacity: 1 },500,'easeOutBack');
				$('.andenes .disc').transition({ x:'0px', opacity: 1 },500,'easeOutBack',function(){
					$('.andenes .b1').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
						$('.andenes .b2').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
							$('.andenes .b3').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
								$('.andenes .b4').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
									$('.andenes .b6').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
										$('.andenes .b5').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
											$('.andenes .b9').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
												$('.andenes .b7').transition({ y:'0px', opacity: 1 },300,'easeOutBack',function(){
													$('.andenes .b8').transition({ y:'0px', opacity: 1 },300,'easeOutBack');
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});

	$('.coms .i1').transition({ opacity:0 },0);
	$('.coms .i2').transition({ opacity:0, y:'-20px', scale:.6 },0);
	$('.coms .i3').fadeOut(0);
	$(".coms").on("inview",function(){
		$('.coms .i1').delay(1000).transition({ opacity:1 },500,function(){
			$('.coms .i2').transition({ opacity:1, y:'0px', scale:1 },4000,'easeOutExpo');
			$('.coms .i3').delay(700).fadeIn(400);
		});
	});

	$('.notif .i1').transition({ opacity:0 },0);
	$('.notif .i2').css({ transformOrigin: '20px 187px' }).transition({ opacity:0, scale:.6 },0);
	$(".notif").on("inview",function(){
		$('.notif .i1').delay(1000).transition({ opacity:1 },500,function(){
			$('.notif .i2').css({ transformOrigin: '20px 187px' }).transition({ opacity:1, scale:1 },1500,'easeOutCirc');
		});
	});

	$('.alarma .i1').transition({ opacity:0 },0);
	$('.alarma .i3, .alarma .i2').fadeOut(0);
	$('.alarma .i4').transition({ opacity:0, y:'-20px', scale:.6 },0);
	$(".alarma").on("inview",function(){
		$('.alarma .i1').delay(1000).transition({ opacity:1 },500,function(){
			$('.alarma .i4').transition({ opacity:1, y:'0px', scale:1 },4000,'easeOutExpo');
			$('.alarma .i3, .alarma .i2').delay(1500).fadeIn(300);
		});
	});

	$('.monitoreo .i1, .monitoreo .i2, .monitoreo .i3').fadeOut(0);
	$(".monitoreo").on("inview",function(){
		$('.monitoreo .i1, .monitoreo .i2, .monitoreo .i3').delay(1000).fadeIn(300);
	});

	$('.emergencia .i1').transition({ opacity:0, y:'-40px' },0);
	$('.emergencia .i3, .emergencia .i2').fadeOut(0);
	$(".emergencia").on("inview",function(){
		$('.emergencia .i1').delay(1000).transition({ opacity:1, y:'0px' },500,'easeOutExpo',function(){
			$('.emergencia .i3, .emergencia .i2').fadeIn(300);
		});
	});

	$('.estacion .reloj').transition({ opacity:0, scale:.5 },0);
	$('.estacion .eco1, .estacion .eco2, .estacion .eco5, .estacion .eco6').transition({ opacity:0, y:'-40px' },0);
	$('.estacion .eco3').transition({ opacity:0, x:'50px', y:'-40px' },0);
	$('.estacion .eco4').transition({ opacity:0, x:'-50px', y:'40px' },0);
	$(".estacion").on("inview",function(){
		$('.estacion .eco2').delay(1000).transition({ opacity:1, y:'0px' },2000,'easeOutExpo');
		$('.estacion .eco1').delay(1500).transition({ opacity:1, y:'0px' },1000,'easeOutExpo',function(){
			$('.estacion .eco3').transition({ opacity:1, x:'0px', y:'0px' },2000,'easeOutExpo');
			$('.estacion .eco4').transition({ opacity:1, x:'0px', y:'0px' },2000,'easeOutExpo');
			$('.estacion .eco5').delay(800).transition({ opacity:1, y:'0px' },800,'easeOutExpo');
			$('.estacion .eco6').delay(400).transition({ opacity:1, y:'0px' },800,'easeOutExpo');
		});
	});

	$('.nav1').click(function(){
		$.scrollTo('#seccion1',500);
		return false;
	});

	$('.nav2').click(function(){
		$.scrollTo('#seccion2',500);
		return false;
	});

	$('.nav3').click(function(){
		$.scrollTo('#seccion3',500);
		return false;
	});

	$('.nav4').click(function(){
		$.scrollTo('#seccion4',500);
		return false;
	});

	$('.nav5').click(function(){
		$.scrollTo('#seccion5',500);
		return false;
	});

	$('.nav6').click(function(){
		$.scrollTo('#seccion6',500);
		return false;
	});

	$('.nav7').click(function(){
		$.scrollTo('#seccion7',500);
		return false;
	});

});
<?php include "php/mail.php" ?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">
<meta name="format-detection" content="telephone=no">
<meta name="robots" content="INDEX,FOLLOW,ARCHIVE" />
<meta name="description" content="">
<meta name="keywords" content=""/>

<link rel="shortcut icon" type="image/x-icon" href="brt/images/favicon.ico">
<link rel="apple-touch-icon" href="brt/images/phone.png">
<link rel="apple-touch-icon" sizes="72x72" href="brt/images/tablet.png">
<link rel="apple-touch-icon" sizes="114x114" href="brt/images/retina.png">

<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,600,300" rel="stylesheet" type="text/css">

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-399268-1"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="brt/css/main.css">
<link rel="stylesheet" href="css/brt_styles.css">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="brt/js/jquery-1.11.1.min.js"></script>
<style type="text/css">
	
	form {
	    position: relative;
	}
	form input, form textarea {
	    display: block;
	    width: 100%;
	    outline: none;
	    background: none;
	    border-width: 1px;
	    border-style: solid;
	    border-color: #B8C5C0;
	    padding: 8px 20px;
	    color: #454545;
	    color: rgba(69, 69, 69, 0.5);
	}

	form input[type=text], form input[type=email], form input[type=tel], form input[type=password] {
	    height: 39px;
	}
	form input[type=submit] {
		background-color: #4AC3E0;
	    width: 140px;
	    height: 50px;
	    font-weight: 600;
	    text-transform: uppercase;
	    -webkit-transition: color 300ms ease-in-out, background 300ms ease-in-out, border-color 300ms ease-in-out;
	    -o-transition: color 300ms ease-in-out, background 300ms ease-in-out, border-color 300ms ease-in-out;
	    transition: color 300ms ease-in-out, background 300ms ease-in-out, border-color 300ms ease-in-out;
	}
	form input[type=submit]:hover {
	    color: #ffffff;
	    background-color: #454545;
	}
	form input[type=text], form textarea, form label {
	    margin-bottom: 14px;
	}
	form textarea {
	    height: 110px;
	    resize: vertical;
	}
	.csstransitions form input[type="checkbox"] {
	    display: none;
	}
	form input[type="checkbox"] {
	    display: inline-block;
	    vertical-align: middle;
	    margin-right: 10px;
	}
	form label {
	    display: inline-block;
	    position: relative;
	    ;
	    cursor: pointer;
	    color: #454545;
	    -webkit-transform: translateZ(0);
	    -ms-transform: translateZ(0);
	    -o-transform: translateZ(0);
	    transform: translateZ(0);
	}
	form label span {
	    display: inline-block;
	    vertical-align: middle;
	    position: relative;
	    width: 13px;
	    height: 13px;
	    border-width: 1px;
	    border-style: solid;
	    border-color: #B8C5C0;
	    margin-top: -5px;
	    margin-right: 10px;
	}
	form label span:before {
	    content: "";
	    position: absolute;
	    top: 1px;
	    right: 1px;
	    bottom: 1px;
	    left: 1px;
	    background-color: #000000;
	    border: 1px solid #81b4a0;
	    opacity: 0;
	}
	form input[type="checkbox"]:checked+span:before {
	    opacity: 0.5;
	}


	#contact {
	    padding: 65px 0 30px;
	}
	#contact .row>div {
	    margin-bottom: 40px;
	}
	#contact h3 {
	    !margin-bottom: 25px;
	}
	#contact p {
	    margin: 3px 0 23px;
	}
	#contact form p {
	    overflow: hidden;
	    margin: 0;
	}
	#contact form a:hover {
	    text-decoration: underline;
	}
	.contact-info, .contact-address {
	    font-weight: 300;
	}
	#contact .contact-address p {
	    margin-bottom: 15px;
	}

	footer {
	    padding: 15px 0 20px;
	    background-color: #3c3c3c;
	    color: #c5c5c5;
	    font-weight: 300;
	}
	.footer-item {
	    margin: 30px 0;
	}
	footer h4 {
	    text-transform: uppercase;
	    color: #ffffff;
	    margin-bottom: 17px;
	}
	#footer-logo {
	    display: inline-block;
	    margin-top: 5px;
	}
	#footer-social {
	    display: inline-block;
	    vertical-align: bottom;
	    margin-left: 20px;
	}
	#footer-social a {
	    font-size: 15px;
	    color: rgba(255, 255, 255, 0.2);
	    margin-left: 3px;
	    -webkit-transition: color 300ms ease-in-out;
	    -o-transition: color 300ms ease-in-out;
	    transition: color 300ms ease-in-out;
	}
	#footer-social a:hover {
	    color: rgba(255, 255, 255, 0.5);
	}
	#footer-social a:first-child {
	    margin-left: 0;
	}
	footer p {
	    margin: 17px 0 20px;
	}
	.tweet article {
	    padding-top: 15px;
	}
	.tweet article:first-child {
	    padding-top: 0;
	}
	.tweet a {
	    color: inherit;
	}
	.tweet a:hover {
	    text-decoration: underline;
	}
	.tweet span {
	    display: block;
	    padding-top: 4px;
	    font-size: 12px;
	    color: #858585;
	}
	.tweet i {
	    color: #ffffff;
	    font-size: 15px;
	    display: inline-block;
	    vertical-align: -1px;
	    margin-right: 10px;
	}
	footer form.style1 {
	    margin: -5px 0 25px;
	}
	footer form.style2 {
	    padding-top: 5px;
	}
	footer input, footer textarea {
	   /*Antes: border-color: #757575;*/ 
	   	border-color: #999999;
	    font-size: 13px;
	    color: #fff;
	}
	footer input[type="text"], footer textarea, footer input[type="email"] {
	    margin-bottom: 9px;
	}
	footer form.style1 input[type="text"] {
	    padding-right: 138px;
	}
	footer input[type="submit"] {
	    width: 120px;
	    height: 39px;
	    color: #fff;
	}
	footer form.style1 input[type="submit"] {
	    position: absolute;
	    top: 0;
	    right: 0;
	}
	footer input[type="submit"]:hover {
	    border-color: #999999 !important;
	}
	footer ul.widget {}
	footer .widget li {
	    position: relative;
	    margin-top: 20px;
	    padding-left: 20px;
	}
	footer .widget li:before {
	    content: "";
	    position: absolute;
	    top: 4px;
	    left: 0;
	    width: 12px;
	    height: 12px;
	    border: 2px solid #ffffff;
	    opacity: 0.5;
	    filter: alpha(opacity=50);
	    -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)';
	    -webkit-border-radius: 50%;
	    -moz-border-radius: 50%;
	    border-radius: 50%;
	}
</style>

<title>Avans | Sistema BRT</title>
</head>
<body>
	<?php include 'includes/header.php'; ?>
	<div class="bigBanner" id="seccion1">
		<div class="cont">
			<p>La solución en automatización<br>de accesos para sistemas BRT</p>
		</div> 
		<?php  if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
			strstr($_SERVER['HTTP_USER_AGENT'],'iPod') ||
			strstr($_SERVER['HTTP_USER_AGENT'], 'iPad'))
			{
				echo '<img src="/brt/images/portada.jpg" class="cover">';
			} else {
				echo '<video autoplay loop poster="/brt/images/portada.jpg" width="100%"><source src="/brt/videos/video.mp4" type="video/mp4" /><source src="/brt/videos/video.webm" type="video/webm"/><source src="/brt/videos/video.ogv" type="video/ogg"/><p>Tu navegador no soporta video en HTML5</p></video>';
			}
		?>
	</div>

	<a id="seccion2"></a>
	<div class="sistema clearfix">
		<article>
			<h2>SISTEMA BRT</h2>
			<p class="gris">En un sistema BRT de transporte público se instalan carriles confinados para el paso exclusivo de autobuses. Éstos carriles facilitan y agilizan el movimiento de los autobuses enun tramo determinado.</p> 
			<p>Se cuenta con estaciones de acceso para los usuarios y por lo general se utilizan estaciones terminales en los dos extremos de la línea de transporte.</p>
		</article>
		<img src="/brt/images/puertas-brt.png" height="527" width="522">
	</div>

	<div class="soluciones">
		<div class="cont clearfix">
			<span>Soluciones AVANS</span>
			<p>Las puertas automáticas permiten generar zonas de comfort al aislar a los usuarios del ruido, contaminación y clima adverso. así mismo, incrementan la seguridad de las estaciones aunado a la imagen de modernidad que representan. Nuestras puertas están especialmente diseñadas para operar en condiciones de uso extremo como lo es el transporte público en zonas urbanas.</p>
		</div>
	</div>

	<a id="seccion3"></a>
	<div class="caract">
		<div class="cont clearfix">
			<div class="tit" style="margin-top: -20px;">Características de puertas de acceso peatonal a estación.</div>
			<br>
			<div class="globe abajo b1">
				<img src="/brt/images/bolita1.png" height="29" width="29">
				<p>Marco de aluminio para protección de usuarios y sellado hemético.</p>
			</div>
			<div class="globe abajo b2">
				<img src="/brt/images/bolita2.png" height="29" width="29">
				<p>Sensor de movimiento y presencia permite el flujo seguro de personas.</p>
			</div>
			<div class="globe abajo b3">
				<img src="/brt/images/bolita3.png" height="29" width="29">
				<p>Control de 3 funciones que permite dejar la puerta abierta/cerrada o en operación automática.</p>
			</div>
			<div class="globe arriba b4">
				<img src="/brt/images/bolita4.png" height="29" width="29">
				<p>Abatimiento manual por emergencia que facilita una evacuación repentina.</p>
			</div>
			<div class="globe abajo b5">
				<img src="/brt/images/bolita5.png" height="29" width="29">
				<p>Cerrojo manual con llave para cierre nocturno.</p>
			</div>
			<div class="globe arriba b6">
				<img src="/brt/images/bolita6.png" height="29" width="29">
				<p>Vidrio templado de 6mm de seguridad y apoyo en ahorro de energía.</p>
			</div>
			<div class="puertas">
				<img src="/brt/images/puerta-ilu-izq.png" class="izq" height="502" width="167">
				<img src="/brt/images/puerta-ilu-der.png" class="der" height="501" width="166">
			</div>
			<img src="/brt/images/torniquetes.png" class="torniquetes" height="327" width="788">
			<img src="/brt/images/piso.png" class="piso" height="319" width="1300">
		</div>
	</div>

	<div class="andenes">
		<div class="cont clearfix">
			<div class="tit">Características puerta automática de andenes.</div>
			<div class="globe arriba b1">
				<img src="/brt/images/cb1.png" height="28" width="27">
				<p>La palanca de pánico abre todas las puertas y envía una alarma visual / auditiva.</p>
			</div>
			<div class="globe abajo b2">
				<img src="/brt/images/cb2.png" height="28" width="27">
				<p>El sistema de comunicación entra autobús y puerta envía una señal cuando llega el autobús y abre la puerta.</p>
			</div>
			<div class="globe abajo b3">
				<img src="/brt/images/cb3.png" height="28" width="27">
				<p>Puerta telescópica de 6 hojas que maximiza el claro libre transitable de 3.20 metros de ancho.</p>
			</div>
			<div class="globe abajo b4">
				<img src="/brt/images/cb4.png" height="28" width="27">
				<p>Banda dentada con alma y refuerzo de acero.</p>
			</div>
			<div class="globe arriba b5">
				<img src="/brt/images/cb5.png" height="28" width="27">
				<p>Sistema de pedestal para señalar al chofer de la presencia de una persona con discapacidad.</p>
			</div>
			<div class="globe abajo b6">
				<img src="/brt/images/cb6.png" height="28" width="27">
				<p>La batería de respaldo permite hasta 100 aperturas/cierres en caso de fallo de energía.</p>
			</div>
			<div class="globe arriba b7">
				<img src="/brt/images/cb7.png" height="28" width="27">
				<p>El cerrojo electrónico mantiene la puerta cerrada cuando no hay un autobús presente. Esto impide que deslicen las pueras manualmente.</p>
			</div>
			<div class="globe arriba b8">
				<img src="/brt/images/cb8.png" height="28" width="27">
				<p>El buzzer o alarma auditiva indica que la puerta está en movimiento.</p>
			</div>
			<div class="globe arriba b9">
				<img src="/brt/images/cb9.png" height="28" width="27">
				<p>El sardinel inferior distribuye la presión del viento hacia diferentes puntos del enmarcado de la puerta y, por ende, evita la vibración de ñas hojas.</p>
			</div>
			<img src="/brt/images/cb10.png" class="disc" height="297" width="93">
			<img src="/brt/images/cb11.png" class="alerta" height="111" width="46">
			<div class="puerta">
				<img src="/brt/images/puerta-a-izq1.png" class="izq1" height="454" width="182">
				<img src="/brt/images/puerta-a-izq2.png" class="izq2" height="457" width="171">
				<img src="/brt/images/puerta-a-der1.png" class="der1" height="454" width="182">
				<img src="/brt/images/puerta-a-der2.png" class="der2" height="457" width="171">
			</div>
		</div>
		<div class="piso"></div>
	</div>

	<a id="seccion4"></a>
	<ul class="feats">
		<li class="coms infoRight clearfix">
			<img src="/brt/images/sist1.png" class="i1" height="288" width="274">
			<img src="/brt/images/sist2.png" class="i2" height="217" width="247">
			<img src="/brt/images/sist3.png" class="i3" height="36" width="20">
			<article>
				<h3>Sistema de comunicación entre puerta automática y autobús.</h3>
				<p>Consiste en la comunicación de un sensor óptico que esta instalado en la puerta exterior y un receptor que se encuentra en el autobús. Cuando el autobús se alinea, activa el sensor y se envía la señal a las puertas para que sean abiertas. Las puertas permanecen abiertas hasta que el autobús se vaya.</p>
			</article>
		</li>
		<li class="notif infoLeft clearfix">
			<img src="/brt/images/notif1.png" class="i1" height="188" width="293">
			<img src="/brt/images/notif2.png" class="i2" height="187" width="150">
			<article>
				<h3>Notificación de persona con necesidades especiales.</h3>
				<p>Una persona con necesidades especiales notifica su presencia al chofer del autobús al presionar un botón especialmente rotulado que envía una señal de activación a un estrobo en el exterior de la puerta.</p>
			</article>
		</li>
		<li class="alarma infoRight clearfix">
			<img src="/brt/images/alarma1.png" class="i1" height="225" width="425">
			<img src="/brt/images/alarma2.png" class="i2" height="50" width="48">
			<img src="/brt/images/alarma3.png" class="i3" height="41" width="89">
			<img src="/brt/images/alarma4.png" class="i4" height="168" width="191">
			<article>
				<h3>Alarma acústica</h3>
				<p>Cuando se aproxima el autobus a la puerta, se activa la Alarma acustica dentro de la estación. Indicando que las puertas están en movimiento. Muy útil para las personas invidentes.</p>
			</article>
		</li>
		<li class="monitoreo infoLeft clearfix">
			<img src="/brt/images/pauto1.png" class="i1" height="185" width="430">
			<img src="/brt/images/pauto2.png" class="i2" height="130" width="64">
			<img src="/brt/images/pauto3.png" class="i3" height="130" width="64">
			<article>
				<h3>Monitoreo Remoto de Puertas Automáticas.</h3>
				<p>Todas las puertas están conectadas a través de un sistema de red local. En el cuarto de control se puede ver el estatus o el historial de cada una de las puertas indicando si estan abiertas o cerradas.</p>
				<p>El operador de la Ecovía puede abrir y/o cerrar las puertas de cualquier estación de forma remota.</p>
			</article>
		</li>
		<li class="emergencia infoRight clearfix">
			<img src="/brt/images/emerg1.png" class="i1" height="194" width="434">
			<img src="/brt/images/emerg2.png" class="i2" height="44" width="42">
			<img src="/brt/images/emerg5.png" class="i3" height="37" width="74">
			<article>
				<h3>Sistema de emergencia.</h3>
				<p>Las estaciones cuentan con una palanca de pánico  que activa una señal audiovisual por medio de una torreta de color rojo y una bocina tipo alarma.</p>
				<p>Cuando la palanca es activada, el sistema de control envía una señal de apertura a todas las puertas con el propósito de facilitar la evacuación de la estación.</p>
				<p>Otra característica de nuestras puertas de acceso peatonal a la estación, es que cuentan con un sistema de abatimiento por emergencia. En caso de necesidad, las puertas se empujan hacia el exterior para facilitar la evacuación.</p>
			</article>
		</li>
	</ul>

	<a id="seccion5"></a>
	<div class="brt">
		<div class="cont">
			<ul class="tres clearfix">
				<li>
					<h4>Sistema BRT</h4>
					<span>Bus Rapid Transit</span>
					<p>Autobús de tránsito rápido </p>
				</li>
				<li>
					<div><img src="/brt/images/sisbrt1.png" height="72" width="191"></div>
					<strong>Carriles Exclusivos para autobuses</strong>
					<p>Tiene como objetivo combinar los carriles de autobuses con 'estaciones' de autobuses de alta calidad.</p>
				</li>
				<li>
					<div><img src="/brt/images/sisbrt2.png" height="91" width="217"></div>
					<strong>Paradas fijas con plataformas</strong>
					<p>Paradas cortas, es decir, abordaje y desabordaje de varias decenas de pasajeros en cortos períodos de tiempo.</p>
				</li>
			</ul>
			<div class="estacion">
				<h5>Funcionamiento del sistema BRT.</h5>
				<ul class="list clearfix">
					<li><span>1</span><p>Llega el camión a tiempo</p></li>
					<li><span>2</span><p>Se abren las puertas automáticamente</p></li>
					<li><span>3</span><p>Abordan los pasajeros</p></li>
					<div class="clear"></div>
					<li><span>4</span><p>El camión se retira de la estación</p></li>
					<li><span>5</span><p>Se cierran las puertas automáticamente</p></li>
				</ul>
				<img src="/brt/images/eco1.png" class="eco1" height="440" width="671">
				<img src="/brt/images/eco2.png" class="eco2" height="1424" width="1852">
				<img src="/brt/images/eco3.png" class="eco3" height="142" width="159">
				<img src="/brt/images/eco4.png" class="eco4" height="143" width="163">
				<img src="/brt/images/eco5.png" class="eco5" height="263" width="450">
				<img src="/brt/images/eco5.png" class="eco6" height="263" width="450">
				<img src="/brt/images/eco7.png" class="nube1" height="137" width="210">
				<img src="/brt/images/eco8.png" class="nube2" height="137" width="210">
				<img src="/brt/images/reloj.png" class="reloj" height="212" width="212">
			</div>
		</div>
	</div>
</body>
<footer>
    <div class="container">
        <div class="row">
        	<div class="col-xs-12 col-sm-7 col-md-8">
        	    <div class="contact-address">
        	        <h4>
        	            <br>
        	            <b>
        	                Nuestra ubicación
        	            </b>
        	        </h4>
        	        <p>
        	            <br>
        	            <b>
        	                México D.F.
        	            </b>
        	            <br>
        	                <i class="ico icon-location rounded_50 base-text-color base-border-color">
        	                </i>
        	                Río Churubusco #407 Col. Unidad Modelo Del. Iztapalapa C.P. 09089 México, D.F.
        	            <i class="ico icon-phone rounded_50 base-text-color base-border-color">
        	            </i>
        	            Tel. +52 (55) 5646 - 0046
        	            <br>
        	            <i class="ico icon-mail rounded_50 base-text-color base-border-color">
        	            </i>
        	            Email:
        	            <a href="mailto:ventas@avans.com">
        	                ventas@avans.com
        	            </a>
        	            <br>
        	            <br>
        	        </p>
        	        <p>
        	            <b>
        	                Monterrey
        	            </b>
        	            <br>
        	                <i class="ico icon-location rounded_50 base-text-color base-border-color">
        	                </i>
        	                León Guzmán No. 1610 Col. Nuevo Repueblo, C.P. 64700 Monterrey, N.L., México
        	            <i class="ico icon-phone rounded_50 base-text-color base-border-color">
        	            </i>
        	            Tel. +52 (81) 1052 3738
        	            <br>
        	            <i class="ico icon-fax rounded_50 base-text-color base-border-color">
        	            </i>
        	            <i class="ico icon-mail rounded_50 base-text-color base-border-color">
        	            </i>
        	            Email:
        	            <a href="mailto:ventas@avans.com">
        	                ventas@avans.com
        	            </a>
        	        </p>
        	        <p>
        	            <br>
        	            <b>
        	                Interior de la República
        	            </b>
        	            <br>
        	            <i class="ico icon-phone rounded_50 base-text-color base-border-color">
        	            </i>
        	            Tel. 01 800 00 AVANS
        	            <br>
        	            <i class="ico icon-mail rounded_50 base-text-color base-border-color">
        	            </i>
        	            Email:
        	            <a href="mailto:ventas@avans.com">
        	                ventas@avans.com
        	            </a>
        	        </p>
        	    </div>
        	</div>
            <div class="col-xs-12 col-sm-5 col-md-4">
                <div class="footer-item">
                    <h4>
                        <b>
                            Envíanos un mensaje.
                        </b>
                        <br>
                        Queremos saber de tu proyecto.
                    </h4>
                    <p>
                    </p>
                    <form method="post">
                        <input name="nombre" placeholder="Nombre" type="text"/>
                        <input name="telefono" placeholder="Telefono" type="text"/>
                        <input name="email" placeholder="E-mail" type="text"/>
                        <!---<input type="text" placeholder="Empresa" /-->
                        <textarea name="mensaje" placeholder="Mensaje" type="text"></textarea>
                        <input class="base-text-color" type="submit" value="Enviar"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12" style="text-align: center;">
                Copyright © 2019 Avance Inteligente SA de CV
            </div>
        	 <div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12" style="text-align: center;">
        	 	<a href="/files/aviso_privacidad.pdf">Aviso de privacidad</a>
        	 </div>
        </div>
    </div>
</footer>
<?php include 'chat.php'; ?>
<script type="text/javascript" src="/brt/js/jquery.touchcarousel-1.2.min.js"></script>
<script type="text/javascript" src="/brt/js/jquery.transit.min.js"></script>
<script type="text/javascript" src="/brt/js/jquery.inview.js"></script>
<script type="text/javascript" src="/brt/js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="/brt/js/avans.js"></script>
</html>
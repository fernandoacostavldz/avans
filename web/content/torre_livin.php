<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre LivIn
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Torre LivIn
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Sin perder el diseño
                            </h2>
                            <p style="text-align: justify;">
                                En este edificio el reto para Avans fue el diseño y la instalación de elevadores acordes al diseño arquitectónico. En este caso se tuvieron que instalar elevadores de mediana capacidad con dimensiones de 90 x 1. 10 metros, para no afectar la velocidad ni el flujo de la transportación.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 2</li>
                                        <li>Velocidad: 1.60 m/s</li>
                                        <li>Capacidad: 630 kg</li>
                                        <li>Paradas: 15</li>
                                        <li>Tipo: Sin cuarto de maquinas</li>
                                        <li>Diseño: pasajeros</li>
                                        <li>Altura de puertas: 2.10 mts</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/TOP/top_prueba.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                },
                {
                    url:"images/proyectos/elevadores/TOP/TOP2-edited-bkp.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.525,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
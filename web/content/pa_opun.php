<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta abatible:
                        </b>
                        Operador Únicamente
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/ABATIBLES/operador/puertas-automaticas-operador-unicamente-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/ABATIBLES/operador/puertas-automaticas-operador-unicamente-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/ABATIBLES/operador/puertas-automaticas-operador-unicamente-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/ABATIBLES/operador/puertas-automaticas-operador-unicamente-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/ABATIBLES/operador/puertas-automaticas-operador-unicamente-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/ABATIBLES/operador/puertas-automaticas-operador-unicamente-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/ABATIBLES/operador/puertas-automaticas-operador-unicamente-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/ABATIBLES/operador/puertas-automaticas-operador-unicamente-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Nuestro operador Power Swing es hidráulico de baja energía, ideal para modernizaciones donde ya se cuenta con una puerta abatible manual y lo único que se requiere es automatizarla. Su operador hidráulico presenta la oportunidad de operar la puerta de forma manual, si se desea, sin dañar los componentes internos del sistema.
                                    </p>
                                    <p>
                                        El operador está diseñado para montarse superficialmente sobre un muro o una placa metálica. El operador Power Swing es muy utilizado en acceso a baños donde contar con una puerta automática elimina la necesidad de tener contacto con la perilla.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Abatible/pa-hojas-dobles.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pa_hd.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pa_hd.php">
                            <p class="post-title base-text-color">
                                Hojas Dobles
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Abatible/pa-hojas-sencillas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pa_hs.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pa_hs.php">
                            <p class="post-title base-text-color">
                                Hojas Sencillas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Abatible/pa-operador-unicamente1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pa_opun.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pa_opun.php">
                            <p class="post-title base-text-color">
                                Operador Únicamente
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Abatible/pa-compartimentalizadoras.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pa_comp.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pa_comp.php">
                            <p class="post-title base-text-color">
                                Compartimentalizadoras
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
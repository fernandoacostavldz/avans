<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Trébol Park
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Trébol Park
            </h2>
        </section>
    
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Equipo: Puerta Giratoria BoonEdam
                                </li>
                                <li>
                                    Modelo: TQ Crystal All Glass
                                </li>
                                <li>
                                   Montura: Subterránea 
                                </li>
                                <li>
                                    Vidrio: Templado / Claro / 12mm
                                </li>
                                <li>
                                    Vidrio curvo: Templado / Claro / 17mm
                                </li>
                                <li>
                                    Acabado: Acero inoxidable
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Diseño de Vanguardia
                            </h2>
                            <p style="text-align: justify;">
                                Complejo de usos mixtos con un diseño de vanguardia e innovación que ofrece un espacio para negocios y hotel. El objetivo del arquitecto era presentar una entrada imponente con la más alta tecnología en acceso. 
                                <br>
                                En este proyecto Avans instaló dos puertas giratorias Full Automatic en la entrada de las oficinas con el fin de facilitar el acceso a ejecutivos de importantes empresas transnacionales.  
                                <br>
                                Las puertas giratorias de Avans están diseñadas para prevenir la entrada de calor del exterior en tiempo de verano, o bien la salida de calor en tiempo de invierno.
                                Esto genera importantes ahorros en la operación del lobby, así como disminuye el tonelaje de aire acondicionado requerido para confort del mismo.
                                <br>
                                Nuestras puertas giratorias cuentan con un botón de disminución de velocidad útil para que las personas con discapacidad puedan entrar con seguridad al edificio, además cuentan con abatimiento de emergencia en todas sus hojas. 
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                // {
                //     url:"images/proyectos/puertas/Trebol_Park/wide6.jpg",
                //     transition:'fade',
                //     scale:'cover',
                //     fade:1000,
                // },
                {
                    url:"images/proyectos/puertas/Trebol_Park/wide2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.9,
                },
                {
                    url:"images/proyectos/puertas/Trebol_Park/wide4.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.1,
                },
                    
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
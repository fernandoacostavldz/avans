<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Alta Seguridad
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php' ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta giratoria:
                        </b>
                        Alta Seguridad
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-7.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-8.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-7.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/alta-seguridad/puertas-automaticas-alta-seguridad-8.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Normalmente, las puertas giratorias pueden permitir el paso de dos o más personas a la vez en cada compartimiento, lo cual puede representar una violación a la seguridad de ciertos edificios. Nuestra puerta RD4A asegura, a través de nuestro Sensor Secure360, que únicamente UNA persona pase a la vez en cada compartimiento. Por lo tanto, en entradas a edificios corporativos que requieren alta seguridad y que cuentan con control de acceso a través de Credenciales de Identificación, nuestra puerta RD4A asegurará que únicamente UNA persona pase con una credencial, brindando un nivel de seguridad incomparable.
                                    </p>
                                    <p>
                                        Cuando dos personas tratan de pasar con una misma credencial, ya sea una detrás de la otra o bien una siendo cargada por la otra, la puerta detectará esta violación de seguridad, parará en el compartimiento seguro, generará una alarma y se regresará automáticamente evitando así la entrada de dos personas a la zona segura del edificio.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-dos-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Dos Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-tres-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Tres Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-cuatro-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_ch.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_ch.php">
                            <p class="post-title base-text-color">
                                Cuatro Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-manuales.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Manuales
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-alta-seguridad1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_as.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_as.php">
                            <p class="post-title base-text-color">
                                Alta Seguridad
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
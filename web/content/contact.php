<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Contacto
    </title>
    <?php include "includes/scripts_top.php"; ?>
    <body>
        <?php include "includes/header.php"; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Conoce Nuestra Empresa
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Empresa
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="map" data-latitude="-99.1264772" data-longitude="19.3659692" data-marker="images/marker.png">
        </section>
        <?php include 'includes/footer.php' ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Escaleras Eléctricas y rampas móviles
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Escaleras Eléctricas y rampas móviles
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/caracteristicas-escaleras2.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/caracteristicas-escaleras.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/banner-escaleras.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="project-content">
                            <h4 class="project-title">
                                Para todo tipo edificio con alto volumen de trafico entre pisos.
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características
                            </h2>
                            <h4>
                                - Mínima infraestructura para su instalación.
                                <br/>
                                - Compatible con todo tipo de arquitectura.
                                <br/>
                                - Diseños especiales.
                                <br/>
                                - Cabinas con accesos múltiples.
                                <br/>
                                - Facilidad de instalación.
                                <br/>
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características Opcionales
                            </h2>
                            <h4 class="project-title">
                                - Uheavy duties
                            </h4>
                            <h4 class="project-title">
                                - Equipso para interpie
                            </h4>
                            <h4 class="project-title">
                                - Anchos especiales en rampa
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Aplicaciones
                    </h2>
                </div>
                <div class="logos-carousel1 logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/escaleras.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/iconos/iconos370x200/i-discapacidad.png"/>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
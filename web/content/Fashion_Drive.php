<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Fashion Drive
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Fashion Drive
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle; text-align: left;">
                                <li>
                                    Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Glass LT
                                </li>
                                <li>
                                    Montura: Dentro del marco / O-X-X-O
                                </li>
                                <li>
                                    Vidrio: Templado / Claro / 9mm
                                </li>
                                <li>
                                    Acabado: Anodizado Natural Mate
                                </li>
                                <li>
                                    Perfil: Europeo
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Alta afluencia de personas
                            </h2>
                            <p style="text-align: justify;">
                                Centro comercial de clase mundial, con más de 80 tiendas con marcas de lujo. Debido a la gran afluencia de personas este proyecto necesitaba accesos automatizados capaces de soportar el tráfico peatonal en todos sus niveles.
                                <br>
                                Las puertas automáticas de los estacionamientos, permiten la entrada ágil a personas con aparatos especiales como sillas de ruedas o andadores y evitan la entrada de los gases tóxicos de los carros. 
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/Fashion_Drive/wide3_1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.8,
                    alignX:.8,
                },
                {
                    url:"images/proyectos/puertas/Fashion_Drive/wide4_1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:0,
                    alignX:0,
                },                                
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
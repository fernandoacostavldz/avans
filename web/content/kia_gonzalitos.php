<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Kia Gonzalitos
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Kia Gonzalitos
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar" >
                            <h2>
                                Especificaciones técnicas
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Glass LT
                                </li>
                                <li>
                                    Montura: Superficial / P-X-X-P
                                </li>
                                <li>
                                    Vidrio: Templado / Claro / 9mm
                                </li>
                                <li>
                                    Acabado: Anodizado Natural Mate
                                </li>
                                <li>
                                    Perfil: Nacional
                                </li>

                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Proyecto
                            </h2>
                            <p style="text-align: justify;">
                                En el edificio de Kia Gonzalitos el reto para Avans fue sustituir las puertas manuales que se tenían por una automática. Las puertas automáticas ofrecen una mayor comodidad para los clientes, son más seguras, más resistentes y proyectan una imagen más moderna y a la vanguardia.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"/images/proyectos/puertas/kia_gonzalitos/kia2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.6,
                    // alignX:.9,
                },
                {
                    url:"/images/proyectos/puertas/kia_gonzalitos/kia3.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.9,
                    // alignX:.9,
                },
                {
                    url:"/images/proyectos/puertas/kia_gonzalitos/kia1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.5,
                    // alignX:.9,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
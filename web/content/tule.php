<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Tulé
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Tulé
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Edificios multifamiliares
                            </h2>
                            <p style="text-align: justify;">
                               Uno de los objetivos principales del desarrollador era ofrecer a los residentes de cada torre la transportación vertical adecuada de los niveles de estacionamiento a los departamentos. 
                               <br>
                               Por lo tanto, el estudio de tráfico de Avans determinó que cada torre necesita dos elevadores para dar un servicio ideal a los usuarios, ajustándose a las dimensiones del proyecto, pues los edificios comparten el estacionamiento.
                               <br>
                               La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y la noma europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 6</li>
                                        <li>Velocidad: 2.0 m/s</li>
                                        <li>Capacidad: 1200 kg</li>
                                        <li>Paradas: 20 a 21</li>
                                        <li>Tipo: Sin cuarto de máquinas</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puertas: 2.10 mts</li>

                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/tule/t1_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:1,
                },
                {
                    url:"images/proyectos/elevadores/tule/t2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:1,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
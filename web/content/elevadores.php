<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Elevadores
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Elevadores
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <br>
        <div class="container">
            <div class="row">
                <!-- Elevador comercial (CCM) -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="elevador-specs.php">
                                <div> 
                                    Elevador Comercial con cuato de máquinas
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/1.jpg"/>
                                </div>
                                <p>
                                    Elevadores recomendados en edificios donde es factible colocar un cuarto de máquinas en la azotea del mismo.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Elevador comercial (SCM) -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="elevador-scdm.php">
                                <div>
                                    Elevador Comercial sin cuarto de máquinas
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/2.jpg"/>
                                </div>
                                <p>
                                    Elevadores que optimizan el espacio al no requerir cuarto de máquinas y desplazarse en un cubo más reducido.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>

            <div class="row">
                <!-- Elevador Industrial Capacidad menor -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="elevador-cm.php">
                                <div> 
                                    Elevador Industrial - capacidad menor
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/3.jpg"/>
                                </div>
                                <p>
                                    Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Elevador residencial -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="equipos-rer.php">
                                <div>
                                    Elevador Residencial
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/7.jpg"/>
                                </div>
                                <p>
                                    Uso en aplicaciones industrial y doméstico para el transporte de material entre diferentes niveles.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>

            <div class="row">
                <!-- Equipos residenciales Plataformas verticales -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="equipos-rpv.php">
                                <div>
                                    Equipos Residenciales - plataformas verticales
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/8.jpg"/>
                                </div>
                                <p>
                                    Para todo tipo edificio con alto volumen de tráfico entre pisos.
                                </p>
                            </a>
                        </div>
                    </div>

                <!-- Equipos residenciales Plataformas verticales -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="equipos-rss.php">
                                <div>
                                    Equipos Residenciales - silla salva-escaleras
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/8.jpg"/>
                                </div>
                                <p>
                                    Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                </p>
                            </a>
                        </div>
                    </div>
                
            </div>

            <div class="row">
                <!-- Equipos residenciales Plataformas verticales -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                        <div class="project-item post-item">
                            <a class="square-link" href="escaleras-eer.php">
                                <div>
                                    Escaleras Eléctricas
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/project_img/3_col/10.jpg"/>
                                </div>
                                <p>
                                    Para todo tipo edificio con alto volumen de trafico entre pisos.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>
        </div>

        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
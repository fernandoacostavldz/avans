<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Elevador Comercial con Cuarto de Maquina
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">Elevador Comercial con Cuarto de Máquina</h2>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 style="margin-top: -25px;">Elevadores recomendados en edificios donde es factible colocar un cuarto de máquinas en la azotea del edificio</h4>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <b>Características</b>
                    <ul  style="list-style-type: circle;">
                        <li>Son utilizados cuando la velocidad es mayor a 2.25m/s.</li>     
                        <li>Usados en edificios con recorridos express.</li>
                        <li>Equipos utilizados para servicio VIP.</li>
                        <li>Requiere de una construcción para alojar la máquina y el controlador en el techo del cubo del elevador.</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <b>Características Opcionales</b>
                    <ul style="list-style-type: circle;">
                        <li>Fase II contra incendios</li>
                        <li>Puertas contra incendio (hasta 90 min)</li>
                        <li>Cumplimiento de norma ANSI A17.1</li>
                        <li>Detección y monitoreo remoto de fallas</li>
                        <li>Velocidad:2.5mps a 5mps</li>
                        <li>Capacidad de carga Mayores de: 5000kg</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <section class="clients base-bg-color_light">
                    <div class="container">
                        <h4>
                            Aplicaciones
                        </h4>
                        <div class="logos-carousel1 logos-carousel_long dots-top">
                            <div class="slide">
                                <img alt="single" src="images/iconos/iconos370x200/edificios_corporativos.png"/>
                            </div>
                            <div class="slide">
                                <img alt="single" src="images/iconos/iconos370x200/edificios_mediana_y_gran_estatura.png"/>
                            </div>
                            <div class="slide">
                                <img alt="single" src="images/iconos/iconos370x200/edificios_departamentales.png"/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
            

        <script type="text/javascript">
            $(function(){
                
                $("#backstretch-photo").backstretch([
                    {
                        url:"images/productos 1170x600/caracteristicas-cuarto-maquina.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.8,
                    },
                  ],{
                    duration:3500,
                  }
                );

                $("#backstretch-photo").backstretch([
                    {
                        url:"images/productos 1170x600/banner-cuarto-maquina.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.8,
                    },
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
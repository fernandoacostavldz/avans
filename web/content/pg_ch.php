<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Cuatro Hojas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puerta giratoria:
                        </b>
                        Cuatro Hojas
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 770x600/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/puertas/PUERTAS 170x140/GIRATORIAS/cuatro-hojas/puertas-automaticas-cuatro-hojas-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        El modelo Four Wing, es el más utilizado en lugares donde el paso de personas es constante. Permite el flujo de personas a la entrada y salida de su edificio, lo que incrementa el tránsito considerablemente.
                                    </p>
                                    <p>
                                        Su sistema de seguridad es el más avanzado del mercado, integrando sensores, switches de compresión y botones de emergencia.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Archivos descargables
                                    </p>
                                    <br/>
                                    <p>
                                        <a class="more-btn" href="pdf/puertas/puertas-giratorias/cuatro-hojas/PPA_giratoria_cuatrohojas_dentromarco_1.3.pdf">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Cuatro Hojas (PDF)
                                        </a>
                                        <br/>
                                        <a class="more-btn" href="pdf/puertas/puertas-giratorias/cuatro-hojas/PA_giratoria_cuatrohojas_dentromarco_1.3.dwg">
                                            <i class="icon-angle-right rounded_50 base-border-color">
                                            </i>
                                            Cuatro Hojas (AutoCad)
                                        </a>
                                        <br/>
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-dos-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Dos Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-tres-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Tres Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-cuatro-hojas.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_ch.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_ch.php">
                            <p class="post-title base-text-color">
                                Cuatro Hojas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-manuales.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Manuales
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-alta-seguridad1.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="pg_as.php">
                                </a>
                            </div>
                        </figure>
                        <a href="pg_as.php">
                            <p class="post-title base-text-color">
                                Alta Seguridad
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
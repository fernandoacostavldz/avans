<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Restaurante En Zona Comercial Arboleda
    </title>
    <?php  include 'includes/scripts_top.php' ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l" >
                        <b>
                            Restaurante en Zona Comercial Arboleda
                        </b>
                    </h2>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide6.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide3.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide4.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide1.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide5.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Restaurante/wide2.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p class="project-title">
                                        Servicios
                                    </p>
                                    <ul class="tags-list">
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Cliente
                                    </p>
                                    <p>
                                        Ejemplo
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Proyecto
                            </h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis hendrerit dui. Donec vitae elit vel sem elementum cursus. Nam consectetur gravida ex vitae varius. In laoreet dolor et lorem tincidunt fringilla. Nulla dictum justo massa, pretium cursus risus lacinia vel. Proin non nisi maximus, mattis erat eget, placerat purus.
                            </p>
                            <p>
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus lacinia ante in massa malesuada, sed consequat tortor iaculis. Praesent ut augue neque. Sed laoreet diam quis tincidunt viverra. Cras semper felis in pulvinar rutrum. Mauris a rhoncus magna. Praesent metus nulla, pharetra sagittis lorem sit amet, ultricies congue enim. Sed at neque consectetur, feugiat eros sed, porta dolor.
                            </p>
                            <p>
                                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi pretium eget risus eget efficitur. Vivamus sagittis nulla ex, sit amet dapibus felis elementum volutpat. Proin at dolor sem. Aliquam ultricies elementum orci non tincidunt. Nullam rhoncus, odio vel tincidunt ullamcorper, velit metus ullamcorper ante, ac pharetra purus elit ut est.
                                <br/>
                                Nunc faucibus quis sapien ut hendrerit. Fusce elit justo, rhoncus nec neque at, facilisis egestas dui. Duis quis hendrerit orci. Duis malesuada scelerisque metus et luctus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent viverra metus at massa varius cursus.
                                <br/>
                                Praesent aliquet vehicula diam ac lacinia. Morbi tincidunt lacus at vulputate imperdiet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In sed sollicitudin risus. Morbi malesuada mauris lacus, in condimentum nibh laoreet eu.
                            </p>
                            <p>
                                Nullam interdum orci quis arcu auctor mollis in ac est. In eleifend leo ut nibh lobortis, in auctor odio aliquet. Morbi suscipit lacinia est, quis feugiat odio. Maecenas eget ex sit amet orci condimentum vehicula nec euismod mi. Nunc gravida ex sed enim congue iaculis. Nulla ullamcorper nunc nec turpis bibendum, ut tincidunt neque pretium.
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otros Proyectos
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Live_Aqua.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Live_Aqua.php">
                            <p class="post-title base-text-color">
                                Grand Fiesta Americana
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Elevadores
                            </li>
                            <li>
                                Puertas Autómaticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Fashion_Drive.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Fashion_Drive.php">
                            <p class="post-title base-text-color">
                                Fashion Drive
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Arboleda.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Arboleda.php">
                            <p class="post-title base-text-color">
                                Arboleda
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Restaurante.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Restaurante.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Restaurante.php">
                            <p class="post-title base-text-color">
                                Restaurante
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus335.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus335.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus335.php">
                            <p class="post-title base-text-color">
                                Equus 335
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/HolyCow.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Holy_Cow.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Holy_Cow.php">
                            <p class="post-title base-text-color">
                                Holy Cow
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus444.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus444.php">
                            <p class="post-title base-text-color">
                                Equus 444
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Trebol_Park.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Trebol_Park.php">
                            <p class="post-title base-text-color">
                                Trebol Park
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/PabellonM.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="PabellonM.php">
                                </a>
                            </div>
                        </figure>
                        <a href="PabellonM.php">
                            <p class="post-title base-text-color">
                                Pabellón M
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Moll del Valle
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Moll del Valle
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Conectar zona antigua con zona nueva
                            </h2>
                            <p style="text-align: justify;">
                                El reto para Avans en este proyecto fue sincronizar el edificio antiguo con el edificio nuevo en las áreas comerciales y el estacionamiento.
                                <br>
                                Para este proyecto Avans realizó un estudio de tráfico para determinar el número de elevadores que se necesitaban. El estudio y la instalación de los elevadores llevó un tiempo aproximado de 24 meses.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li></li>Cantidad: 5</li>
                                        <li>Velocidad: 1.60 m/s</li>
                                        <li>Capacidad:630 a 1250 kg</li>
                                        <li>Paradas: 3 a 11</li>
                                        <li>Tipo: Sin cuarto de maquinas</li>
                                        <li>Diseño: pasajeros- panorámico</li>
                                        <li>Puertas: 2.10 mts</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/moll_del_valle/mv_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.4,
                },
                {
                    url:"images/proyectos/elevadores/moll_del_valle/moll_del_valle_2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.2,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
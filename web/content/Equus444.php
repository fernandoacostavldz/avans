<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Equus444
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Equus 444
                        </b>
                    </h2>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide1.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide2.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide3.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide4.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide5.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/Equus444/wide6.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p class="project-title">
                                        Servicios
                                    </p>
                                    <ul class="tags-list">
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                Placeholder
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p class="project-title">
                                        Cliente
                                    </p>
                                    <p>
                                        Ejemplo
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Producto
                            </h2>
                            <p>
                                Ubicado en San Pedro Garza García, en Nuevo León, en una exclusiva zona corporativa, cerca de grandes avenidas, centros comerciales y restaurantes.
                            </p>
                            <p>
                                El edificio Equus 444 es un excepcional proyecto en el cual se encuentran oficinas AAA, este distintivo proyecto ofrece seguridad, eficiencia y sobre todo exclusividad para las personas que encuentran aquí el mejor aliado para albergar su empresa.
                            </p>
                            <p>
                                El edificio Equus 444 es un excepcional proyecto en el cual se encuentran oficinas AAA, este distintivo proyecto ofrece seguridad, eficiencia y sobre todo exclusividad para las personas que encuentran aquí el mejor aliado para albergar su empresa.
                            </p>
                            <p>
                                Al tener muchas oficinas, hace que un número importante de personas tengan sus empleos en este lugar, las cuales transitan principalmente ejecutivos que necesitan salir y entrar del edificio de una forma rápida, fácil y en caso de cargar con documentos o equipos de cómputo, necesitan una forma fácil y segura de transitar. También muchos clientes entran y salen durante todo el día, por lo que nuestras puertas automáticas generan una importante solución en el flujo de personas.
                            </p>
                            <p>
                                Dentro de este proyecto también se instalaron torniquetes de la más alta tecnología, los cuales ayudan al control de personas que ingresan al edificio evitando que personas ajenas o que carezcan de los accesos necesarios puedan acceder libremente por los espacios del edificio.
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                        <h2>
                            Dirección
                        </h2>
                    </aside>
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otros Proyectos
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Live_Aqua.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Live_Aqua.php">
                            <p class="post-title base-text-color">
                                Grand Fiesta Americana
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Elevadores
                            </li>
                            <li>
                                Puertas Autómaticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Fashion_Drive.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Fashion_Drive.php">
                            <p class="post-title base-text-color">
                                Fashion Drive
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Arboleda.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Arboleda.php">
                            <p class="post-title base-text-color">
                                Arboleda
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Restaurante.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Restaurante.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Restaurante.php">
                            <p class="post-title base-text-color">
                                Restaurante
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus335.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus335.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus335.php">
                            <p class="post-title base-text-color">
                                Equus 335
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/HolyCow.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Holy_Cow.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Holy_Cow.php">
                            <p class="post-title base-text-color">
                                Holy Cow
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Equus444.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Equus444.php">
                            <p class="post-title base-text-color">
                                Equus 444
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="Trebol_Park.php">
                                </a>
                            </div>
                        </figure>
                        <a href="Trebol_Park.php">
                            <p class="post-title base-text-color">
                                Trebol Park
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/Portadas1/PabellonM.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" href="PabellonM.php">
                                </a>
                            </div>
                        </figure>
                        <a href="PabellonM.php">
                            <p class="post-title base-text-color">
                                Pabellón M
                            </p>
                        </a>
                        <ul class="tags-list">
                            <li>
                                Puertas Automáticas
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <?php include "includes/footer.php"; ?>
    <?php include "includes/scripts_bottom.php"; ?>
    <?php include "chat.php"; ?>
</html>
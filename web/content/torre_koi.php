<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Torre KOI
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Torre KOI
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>Equipo: Avans</li>
                                <li>Modelo: Glass LT</li>
                                <li>Montura: Dentro del marco / O-X-X-O</li>
                                <li>Vidrio: Templado / Claro / 9mm</li>
                                <li>Acabado: Anodizado Natural Mate</li>
                                <li>Perfil: Nacional</li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Torre de oficinas y departamentos
                            </h2>
                            <p style="text-align: justify;">
                                Para resolver el desafío Avans tuvo que cambiar los vidrios de antepecho para que pudieran ayudar con la carga del operador y las hojas deslizantes. Se evitó la colocación de vigas o columnas que pudieran afectar la imagen del edificio.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
        <script type="text/javascript">
            $(function(){
                $("#backstretch-photo").backstretch([
                    {
                        // url:"/images/proyectos/pabellonm/pabellonm.jpg?width={width}&height={height}",
                        url:"/images/proyectos/puertas/torre_koi/KOI_1400.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.8,
                    },
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Andamar
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Andamar LifeStyle Center
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar" >
                            <h2>
                                Beneficios 
                            </h2> 
                            <ul style="list-style-type: circle;">
                            <li>Ahorro de energía eléctrica</li>    
                            <li>Puertas giratorias siempre abiertas, siempre cerradas que permiten conservar el aire acondicionado dentro del edificio.</li>
                            <li>Puertas totalmente de cristal, para evitar la corrosión con el agua del mar</li>
                            <li>Puertas abatibles para mayor comodidad de personas en condiciones especiales.</li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Puertas con vista al mar
                            </h2>
                            <p style="text-align: justify;">
                                En este complejo comercial Avans instaló 15 puertas, 8 giratorias, 2 automáticas deslizantes y 5 abatibles. El reto principal era conservar el diseño arquitectónico del edificio y que las puertas, ubicadas en zonas estratégicas del edificio, fueran funcionales para las entradas y las salidas tanto de los clientes como de los meseros, pues este recinto cuenta con terrazas al aire libre donde los usuarios pueden disfrutar de una vista al mar, mientras saborean su bebida favorita.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"/images/proyectos/puertas/andamar/andamar1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.6,
                },
                {
                    url:"/images/proyectos/puertas/andamar/andamar2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.65,
                },
                {
                    url:"/images/proyectos/puertas/andamar/andamar3.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.55,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
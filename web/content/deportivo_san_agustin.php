<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Deportivo San Agustín
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Deportivo San Agustín
            </h2>
        </section>
    
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Slide
                                </li>
                                <li>
                                    Montura:  Cristal templado, zoclo superior
                                </li>
                                <li>
                                    Hojas: Dos hojas OHC
                                </li>
                                <li>
                                    Perfil: Nacional
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Puertas que conservan el vapor de la sauna
                            </h2>
                            <p style="text-align: justify;">
                               Una de las necesidades que tenía este club deportivo era en el área de baños y sauna para caballeros. El reto aquí era contar con puertas que permitirán conservar el vapor de la sauna, sin que se los marcos se dañaran por la humedad, pues los cristales pasaban por una ranura de tabla roca.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/deportivo_san_agustin/deportivo2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                    // alignX:.6,
                },
                {
                    url:"images/proyectos/puertas/deportivo_san_agustin/deportivo1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                    // alignX:.5,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre top
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
<!-- 
        <div class="col-md-12">
            <ul id="pictures-demo">
              <li title="Torre TOP">
                <img src="images/proyectos/elevadores/TOP/TOP2.jpg" alt="Torre TOP">
              </li>
              <li title="Proyecto 2 ">
                <img src="images/proyectos/elevadores/TOP/TOP 3.jpg" alt="demo1_3">
              </li>
            </ul>
        </div> -->

<!--         <section id="content-container" style="padding-top: 0px;">
        </section>  -->
        <section class="intro-section text-center" id="intro-fullwidth">
            <!-- <div class="owlSliderImg" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="true" data-nav="true" id="intro-media"> -->
                <!-- <div class="slide-backstretch" data-backstretch-img="images/proyectos/elevadores/TOP/TOP1-600.jpg"> -->
                <!-- </div> -->
               <!--  <div class="slide-backstretch" data-backstretch-img="images/proyectos/elevadores/TOP/TOP2-edited.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/proyectos/elevadores/TOP/TOB-Close Up-Oficinas.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/proyectos/elevadores/TOP/TOB-Departamentos-Close Up-Opcion 01.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/proyectos/elevadores/TOP/TOB-Departamentos-Opcion 01.jpg">
                </div> 
                </div> -->
        </section>
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Torre T.OP
            </h2>
        </section>
        
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Proyecto
                            </h2>
                            <p style="text-align: justify;">
                            Es un proyecto vertical compuesto por dos torres (Top y Nest) que definen su uso mixto en oficinas, hotel, área comercial y complejo habitacional. Ambas torres están unidas por un estacionamiento. 
                            A través de un estudio de tráfico Avans determinó qué tipo de transportación vertical necesitaban en estos edificios, de esta forma determinó el tipo de elevador, la capacidad y la velocidad en cada sección del edificio.
                            El servicio incluye evaluación constante de la calidad en la transportación, sistema de asignación de destino desde el lobby.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 25</li>
                                        <li>Velocidad: 2.0 a 7.0 m/s</li>
                                        <li>Capacidad: 1000-1600KG</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Puertas: 2.10-2.40</li>
                                        <li>Pardas: 10 A 104</li>
                                        <li>Tipo: C/S cuarto de máquina</li>    
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Escaleras
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 2 </li>
                                        <li>Interior: si</li>
                                        <li>Escalones de desembarque: 3 y 2</li>
                                        <li>Economizador de energía: sI</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
</html> 
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en"> <!--<![endif]-->
    <title>Avans - Puertas Curvas</title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l"><b>Puertas Curvas</b></h2>

                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>Productos</li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div id="project-slider_big" class="project-slider">
                                 <div><img src="images/puertas/puerta-curva.jpg" alt="single" /></div>
                                <div><img src="images/puertas/Curvas/pc-puertas-curvas.jpg" alt="single" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside id="sidebar" class="project-info">
                            <h2>Características</h2>
                            <ul>
                                <li> 
                                    <p>
                                       Nuestro modelo de puerta deslizante curvo es sinónimo de exigencia y cuidado en el diseño arquitectónico. Comúnmente utilizado por arquitectos que buscan diferenciar la entrada de su edificio, la puerta curva combina perfectamente todo tipo de fachadas.
                                    </p>

                                    <p>
                                        Su diseño tridimensional permite jugar con el ancho, altura y profundidad de su entrada. Escoja entre la interminable variedad de colores y acabados para crear una entrada impactante.
                                    </p>
                                </li>
                                <li>
                                    <p class="project-title">Archivos descargables</p>
                                    <br/>
                                    <p> 
                                        <a class="more-btn" href="pdf/puertas/puertas-curvas/dos-hojas/PA_abatible_hojasencilla_superficial_4.4.pdf"><i class="icon-angle-right rounded_50 base-border-color"></i> Soble SA P-SX-SX-P (PDF)</a><br/>
                                       
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>

                    
                </div>
            </div>
        </section>
        <section class="posts-container">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Otras puertas
                    </h2>
                </div>
                <div id="project-slide">
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/Deslizantes/pd-marco-aluminio.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_deslizantes.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_deslizantes.php">
                            <p class="post-title base-text-color">
                                Puertas Deslizantes
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-abatible.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_abatibles.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_abatibles.php">
                            <p class="post-title base-text-color">
                                Puertas Abatibles
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-curva.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_curvas.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_curvas.php">
                            <p class="post-title base-text-color">
                                Puertas Curvas
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-giratoria.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_giratorias.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_giratorias.php">
                            <p class="post-title base-text-color">
                                Puertas Giratorias
                            </p>
                        </a>
                    </div>
                    <div class="project-item post-item">
                        <figure class="img-container">
                            <img alt="single" class="img-responsive" src="images/puertas/puerta-plegadiza.jpg"/>
                            <div class="post-item__description">
                                <a class="icon-plus" data-gallery="f_project" href="puertas_plegadizas.php">
                                </a>
                            </div>
                        </figure>
                        <a href="puertas_plegadizas.php">
                            <p class="post-title base-text-color">
                                Puertas Plegadizas
                            </p>
                        </a>
                    </div>
                    
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
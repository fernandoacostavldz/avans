<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Colegio Inglés
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Colegio Inglés
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Edificio accesible para todos
                            </h2>
                            <p style="text-align: justify;">
                                Este colegio es uno de los más emblemáticos en Monterrey y su área metropolitana.
                                <br>
                                El colegio es inclusivo y con el elevador de pasajeros que instaló Avans los alumnos pueden transportarse mientras disfrutan de una increíble vista panorámica.
                                <br>
                                En este proyecto Avans hizo un estudio de tráfico para determinar el tipo de elevador que los estudiantes y personal administrativo necesitaban. Se decidió instalar un elevador dentro de un cubo de cristal cilíndrico con vista panorámica. Tiempo aproximado de análisis e instalación 12 meses.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 1</li>
                                        <li>Velocidad: 1 m/s</li>
                                        <li>Capacidad: 1250 kg</li>
                                        <li>Diseño: Panorámico de pasajeros</li>
                                        <li>Puerta: 2.10 mts</li>
                                        <li>Paradas: 4</li>
                                        <li>Tipo: S/máquinas</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/colegio_ingles/colegio_ingles_2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.14,
                },
                // {
                //     url:"images/proyectos/elevadores/colegio_ingles/CI 1400.jpg",
                //     transition:'fade',
                //     scale:'cover',
                //     alignY:.6,
                //     fade:1000,
                // },
                {
                    url:"images/proyectos/elevadores/colegio_ingles/ci2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.15,
                },
                {
                    url:"images/proyectos/elevadores/colegio_ingles/ci3_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },
                {
                    url:"images/proyectos/elevadores/colegio_ingles/ci4_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
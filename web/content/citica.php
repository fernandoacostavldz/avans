<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Citica
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Citica
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Proyectos de usos mixtos
                            </h2>
                            <p style="text-align: justify;">
                                El reto para Avans fue adaptar los cuartos de máquina al espacio que el proyecto consideró en su fase original, sólo había un 50% del espacio necesario por lo que tuvo que adaptarse un equipo especial. Los elevadores ya estaban determinados por el cliente en cuanto al número, capacidad y velocidad.
                                <br>
                                La nrma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 7</li>
                                        <li>Velocidad: 1.00 a 2.50 m/s</li>
                                        <li>Capacidad: 1000 y 1600 kg</li>
                                        <li>Paradas: 2 a 24</li>
                                        <li>Tipo: Sin cuarto de máquinas</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puertas: 2.40 mts</li>
                                        <!-- <li>Recorrido total:  88.50 mts <b>Pendiente</b></li> -->
                                        <!-- <li>Recorridos express: 128.8 mts <b>Pendiente</b></li> -->
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                // {
                //     url:"images/proyectos/elevadores/CITICA/c 1400.jpg",
                //     transition:'fade',
                //     scale:'cover',
                //     fade:1000,
                //     alignY:.45,
                // },
                {
                    url:"images/proyectos/elevadores/citica/citica_2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.6,
                },
              ],{
                duration:1000,
              }
            );
        });
    </script>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Torniquete Cuerpo Entero
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Torniquete:
                        </b>
                        Cuerpo Entero
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/cuerpo-entero/torniquetes-cuerpo-entero-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/cuerpo-entero/torniquetes-cuerpo-entero-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/cuerpo-entero/torniquetes-cuerpo-entero-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/cuerpo-entero/torniquetes-cuerpo-entero-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/cuerpo-entero/torniquetes-cuerpo-entero-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/cuerpo-entero/torniquetes-cuerpo-entero-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/cuerpo-entero/torniquetes-cuerpo-entero-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/cuerpo-entero/torniquetes-cuerpo-entero-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Torniquetes de máxima seguridad y aplomo, el modelo Full Height está diseñado para proveer años de protección en su entrada sin mantenimiento alguno.
                                    </p>
                                    <p>
                                        Este torniquete incluye entradas para cualquier tipo de control de accesos electrónico.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_torniquetes.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
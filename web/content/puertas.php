<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Puertas Autómaticas
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puertas Automáticas
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        
                        <li>
                            Proyectos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <br>
        <div class="container">
            <div class="row">
                <!-- Puertas Deslizantes -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="puertas_deslizantes.php">
                                <div> 
                                    Puertas Deslizantes
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/puerta-deslizable.jpg"/>
                                </div>
                                <p>
                                    Una excelente forma de dar la bienvenida garantizando modernidad, comodidad, servicio, control de trafico, reducción de costos y seguridad.
                                </p>
                            </a>
                        </div>
                    </div>
                <!-- Puertas Abatibles -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="project-item post-item">
                            <a class="square-link" href="puertas_abatibles.php">
                                <div>
                                    Puertas Abatibles
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/puerta-abatible.jpg"/>
                                </div>
                                <p>
                                    Ofrece flexibilidad de optimizar espacios, con diseños robustos y una fácil apertura.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>
            <div class="row">
                <!-- Puertas Giratoriras -->
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                        <div class="project-item post-item">
                            <a class="square-link" href="puertas_giratorias.php">
                                <div>
                                    Puertas Giratoriras
                                </div>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/puerta-giratoria.jpg"/>
                                </div>
                                <p>
                                    Abiertas para las personas que pasan por ella, pero cerradas a la intemperie, contaminación del aire y acústica.
                                </p>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
 <!--        <section id="portfolio-container">
            <div class="container">
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="puertas_curvas.php">
                                <p class="post-title base-text-color">
                                    Puertas Curvas
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/puerta-curva.jpg"/>
                                    <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="puertas_curvas.php">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <li>
                                        Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>

                    <div class="element col-xs-12 col-sm-6 col-md-4 idnetity">
                        <div class="project-item post-item">
                            <a href="puertas_plegadizas.php">
                                <p class="post-title base-text-color">
                                    Puertas Plegadizas
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/puerta-plegadiza.jpg"/>
                                     <div class="post-item__description">
                                        
                                        <div class="link-container">
                                            <a class="icon-plus" href="puertas_plegadizas.php">
                                            </a>
                                        </div>
                                        
                                    </div> 
                                </div>
                                <ul class="tags-list">
                                    <li>
                                        Para todo tipo edificio con alto volumen de tráfico entre pisos.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
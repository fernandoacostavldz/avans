        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Live_Aqua.php">
                                <p class="post-title base-text-color">
                                    Grand Fiesta Americana 
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                                    <div class="post-item__description">
                                    </div>

                                </figure>
                                <ul claplugin ss="tags-list">
                                    <li>
                                        <p>Sus puertas refuerzan el glamour y la modernidad del edificio, además ayudan a disminuir la entrada de aire caliente, lo que permite un ahorro de energía.</p>
                                        <br>
                                        <!-- <p>Puertas automáticas que refuerzan el glamour y la modernidad del edificio, además ayudan a disminuir la entrada de aire caliente, lo que permite un ahorro de energía.</p> -->
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Fashion_Drive.php">
                                <p class="post-title base-text-color">
                                    Fashion Drive
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                                    <div class="post-item__description">
                                    </div>

                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        <p>
                                            Un centro comercial necesita puertas capaces de soportar la gran afluencia de personas en todos sus niveles.
                                            <!-- Sus puertas automáticas son capaces de soportar la gran afluencia de personas en todos sus niveles.  -->
                                            <br>
                                            <br>
                                        </p>
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Arboleda.php">
                                <p class="post-title base-text-color">
                                    Arboleda
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                                    <div class="post-item__description">
                                    </div>

                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        <p>
                                            Este proyecto cuenta con un diseño rústico único, por eso se instalaron puertas automáticas con hojas de madera modelo Slide, montadas de manera superficial.
                                            <!-- En este proyecto se utilizaron puertas automáticas con hojas de madera modelo Slide, montadas de manera superficial. -->
                                            <br>
                                        </p> 
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Holy_Cow.php">
                                <p class="post-title base-text-color">
                                    Holy Cow
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/proyectos/holy_cow/holy_cow_edited.jpg"/>
                                    <div class="post-item__description">
                                    </div>

                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        <p>Accesos automáticos que benefician a los comensales porque pueden disfrutar del área exterior sin sufrir las inclemencias del clima; mientras que los meseros transportan la comida de forma rápida y sencilla</p>
                                        <!-- <p>Accesos automáticos que benefician a los comensales porque pueden disfrutar del área exterior sin sufrir las inclemencias del clima; mientras que los meseros transportan la comida de forma rápida y sencilla.</p> -->
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="p_pabellonm.php"> 
                                <p class="post-title base-text-color">
                                    Pabellon M
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/proyectos/pabellonm/pabellonm_570x300.jpg"/>
                                    <div class="post-item__description">
                                    </div>

                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        <p>Este complejo cuenta con una puerta deslizante contra huracanes capaz de resistir vientos de hasta 250 km/hr. La puerta tiene un alma de acero dentro de los perfiles de aluminio y cuenta con junquillos sólidos para mayor resistencia</p>
                                        <!-- <p>Aunque Monterrey no está en una zona de huracanes, la madre naturaleza es muy impredecible y Monterrey ya ha sufrido los estragos de huracanes peligrosos como Gilberto y Alex.</p> -->
                                    </li>   
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Trebol_Park.php">
                                <p class="post-title base-text-color">
                                    Trebol Park
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        <p>Entrada imponente con la más alta tecnología en los accesos y un diseño vanguardista e innovador.</p>
                                        <!-- <p>Complejo de usos mixtos con un diseño de vanguardia e innovación que ofrece un espacio para negocios y hotel. El objetivo del arquitecto era presentar una entrada imponente con la más alta tecnología en acceso. </p> -->
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>  
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Equus.php">
                                <p class="post-title base-text-color">
                                    Equus 444 y 335
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Acceso seguro estos edificios cuentan con una accesibilidad completa, pues tienen puertas giratorias y torniquetes que previenen la entrada de personas ajenas al edificio.
                                        <!-- Accesibilidad completa con puertas giratorias y torniquetes que previenen la entrada de personas ajenas al edificio -->
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>  
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="kia_gonzalitos.php">
                                <p class="post-title base-text-color">
                                    KIA Gonzalitos
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Puerta acorde con la fachada.
                                        <br>  
                                        Para todos los edificios lo más importante es la fachada y junto con esta, la puerta que dará la bienvenida a los visitantes.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="capital_grille.php">
                                <p class="post-title base-text-color">
                                    The Capital Grille
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Puertas automatizadas a la medida del cliente, que permiten conservar el estilo vanguardista del restaurante.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="deportivo_san_agustin.php">
                                <p class="post-title base-text-color">
                                    Deportivo San Agustín
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        En el Deportivo San Agustín los clientes necesitan puertas cómodas, elegantes, vanguardistas y funcionales.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="deportivo_san_agustin.php">
                                <p class="post-title base-text-color">
                                    Planta Skwinkles
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <div class="post-item__description">
                                    </div>
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        La planta Skwinkles cuenta con puertas automáticas que garantizan la seguridad y la comodidad de la planta.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
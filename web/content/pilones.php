<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Pilones
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Pilones
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_7.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_8.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_9.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 770x600/pilones_10.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_4.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_5.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_6.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_7.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_8.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_9.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/PILON 170x140/pilones_10.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Este equipo comúnmente utilizado en lugares donde se requiera alta seguridad en el control vehicular, es discreto en su diseño pero increíblemente seguro y aguantador en su uso.
                                    </p>
                                </li>
                                <li>
                                    <div id="video-section">
                                        <div class="row" style="padding-left: 35px; padding-right: 35px; padding-bottom: 40px;">
                                            <div id="player">
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 35px; padding-right: 35px; padding-bottom: 40px;">
                                            <div id="player1" style="float: bottom;">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
        </section>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto PUNTACERO
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                PUNTACERO
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Escaleras eléctricas a la intemperie
                            </h2>
                            <p style="text-align: justify;">
                                Complejo de usos mixtos ubicado en el centro de la ciudad de Monterrey, que ofrece estilo de vida, entretenimiento, compras y restaurantes en un mismo lugar.
                                <br>
                                En este proyecto, el reto para Avans fue establecer la distribución adecuada que permitiera una eficiente transportación vertical (elevadores y escaleras eléctricas) en áreas como estacionamiento, hotel, oficinas y los diferentes usos del complejo. Así como los accesos públicos a la zona comercial.
                                <br>
                                Además, cuenta con escaleras eléctricas para la transportación vertical de un tráfico intenso.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 13</li>
                                        <li>Velocidad: 0.50 a 3.50 m/s</li>
                                        <li>Capacidad: 900 a 1350 kg</li>
                                        <li>Diseño:  pasajeros y servicio</li>
                                        <li>Altura de puertas: 2.10 mts</li>
                                        <li>Paradas: 4 a 17</li>
                                        <li>Tipo: Con cuarto y sin cuarto de máquina</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/puntacero/pc_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.6,
                },
                {
                    url:"images/proyectos/elevadores/puntacero/pc2_1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignX:1,
                    alignY:.6
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
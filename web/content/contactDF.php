<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en"> <!--<![endif]-->
	<title>Avans - Contacto</title>
    <?php include 'includes/scripts_top.php'; ?>
	<body>
        <?php include 'includes/header.php'; ?>
		<section id="headline">
			<div class="container">
				<div class="section-title clearfix">
					<h2 class="fl-l"><b>Conoce Nuestra Empresa</b></h2>

					<ul id="breadcrumbs" class="fl-r">
						<li><a href="index.php">Inicio</a></li>
						
						<li>Empresa</li>
					</ul>
				</div>
			</div>
		</section>
        <section class="map" data-longitude="19.3659692" data-latitude="-99.1264772" data-marker="images/marker.png">
        </section>

        <section id="headline" style="color: transparent;">
            <div class="container">
                <div class="section-title clearfix">
                    <a href="https://www.google.com/maps/place/19%C2%B021'57.5%22N+99%C2%B007'35.3%22W/@19.3659692,-99.1286659,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d19.3659692!4d-99.1264772">
                        <h2 class="fl-l" >Oficina México D.F.</h2>
                    </a>
                </div>
            </div>
        </section>

        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
	</body>
</html>
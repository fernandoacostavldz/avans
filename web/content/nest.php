<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre Nest
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Nest
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Complejo habitacional
                            </h2>
                            <p style="text-align: justify;">
                                Para Avans el reto en este edificio fue determinar la calidad de la transportación vertical a las personas de cada edificio, diseñando el número de elevadores que necesita el proyecto.
                                <br>
                                <br>
                                En todo momento se cuido la privacidad de los departamentos, pues ambos edificios comparten el estacionamiento.
                                <br>
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 3</li>
                                        <li>Velocidad: 3.50 m/s</li>
                                        <li>Capacidad: 1300 a 1600 kg</li>
                                        <li>Paradas: 36</li>
                                        <li>Tipo: Con cuarto de máquinas</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puertas: 2.30 mts</li>
                                        <!-- <li>Recorrido total: <b>PENDIENTE</b></li> -->
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                [
                    {
                        width:1080,
                        url:"images/proyectos/elevadores/nest/n_1400.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.5,
                        pixelRatio: "auto",
                    },
                    {
                        width:1080,
                        url:"images/proyectos/elevadores/nest/n2_1400.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.8,
                        pixelRatio: "auto",
                    },
                ],

                [
                    {
                        width:320,
                        url:"images/proyectos/elevadores/nest/nest_vertical.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.8,
                        pixelRatio: "auto",
                    }
                ]
              ],{
                alwaysTestWindowResolution:true,
                duration:3500,
              }
            );
        });
    </script>
</html>
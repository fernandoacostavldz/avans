<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre T.OP
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Torre T.OP
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                El edificio más alto de Latinoamérica
                            </h2>
                            <p style="text-align: justify;">
                                Proyecto vertical compuesto por dos torres llamada Torres Obispado (Top) y Nest que definen su uso mixto en oficinas, hotel, área comercial y complejo habitacional. Ambas torres están unidas por el estacionamiento. 
                                <br>
                                <br>
                                A través de un estudio de tráfico Avans se determinó la cantidad y velocidad de elevadores para ofrecer una calidad de transportación vertical ideal.
                                <br>
                                <br>
                                El conjunto de elevadores cuenta con un sistema que evalúa en todo momento la calidad de la transportación vertical, para así optimizar los consumos de energía de los elevadores y cuenta con el sistema de asignación de destino desde el Loby (DBD). 
                                <br>
                                <br>
                                Además, cuenta con las escaleras eléctricas para dividir el loby de oficinas altas y el loby oficinas bajas.
                                <br>
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 25</li>
                                        <li>Velocidad: 2.00 a 7.00 m/s</li>
                                        <li>Capacidad: 1000-1600 kg</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puertas: 2.10 mts para estacionamiento,  2.30 mts para oficinas y dptos.</li>
                                        <li>Pardas: 10 a 50</li>
                                        <li>Tipo: Con y sin cuarto de máquina</li>
                                        <li>Recorridos totales: hasta 283.00 mts</li>
                                        <!-- <li>Recorridos express: hasta <b>Pendiente</b></li> -->
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Escaleras
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 2</li>
                                        <li>Interior: si</li>
                                        <li>Escalones de desembarque: 2</li>
                                        <li>Con economizador de energía</li>
                                        <li>Con altura de 4.50 mts</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/top/top_prueba.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                },
                {
                    url:"images/proyectos/elevadores/top/TOP2-edited-bkp.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.525,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
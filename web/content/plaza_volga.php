<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Plaza Volga
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Plaza Volga
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Equipos adecuados
                            </h2>
                            <p style="text-align: justify;">
                                En esta plaza el reto fue diseñar la transportación vertical para la plaza que cuenta con tres niveles de locales y 4 sótanos de estacionamiento.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 1</li>
                                        <li>Velocidad: 1.0 m/s</li>
                                        <li>Capacidad: 1000 kg</li>
                                        <li>Paradas: 6</li>
                                        <li>Tipo: Sin cuarto de máquinas</li>
                                        <li>Diseño: pasajeros</li>
                                        <li>Altura de puertas: 2.10 mts</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/PLAZA VOLGA/plaza volga.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
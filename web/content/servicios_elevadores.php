<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Servicios Elevadores
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Servicios de Elevadores
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Servicios
                            </a>
                        </li>
                        <li>
                        	Elevadores
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container" style="padding-top: 15px;">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Asesoría Personalizada en la selección de Equipos
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/1.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Proporcionamos asesoría profesional en la selección de los elevadores, hacemos especial  hincapié en el flujo de personas que tendrá el edificio y las características de los elevadores necesarios para soportar la afluencia de personas.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 UI">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Estudios de Tráfico
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/2.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    El estudio de tráfico tiene por objeto determinar las características y cantidad de elevadores necesarios para realizar una transportación vertical óptima en el edificio. Este estudio de tráfico se basa en estándares internacionales y toma en consideración población, número de pisos y clasificación del edificio. Los resultados que el estudio de tráfico arroja, son la velocidad, cantidad y capacidad de los elevadores.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Elaboración de Catálogo de Conceptos / Presupuestos
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/3.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Entendemos la complejidad que implica el armar un catálogo de conceptos, por lo que colaboramos en la entrega del mismo para aquellos especificadores interesados en integrar nuestros equipos a sus proyectos.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 interactive">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Vista técnica
                            </p>
                            <br>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/4.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Ofrecemos total apoyo en la selección de puertas automáticas, háblenos y con todo gusto visitaremos su obra para tomar medidas y proporcionarle información técnica acerca de nuestros productos y su instalación.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 creative">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Servicios de Elevadores /
                                <b>
                                    Monitoreo remoto
                                </b>
                            </p>
                            <br>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/5.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Nuestros equipos tienen la opción de ser monitoreados desde nuestras oficinas. Contamos con la capacidad de hacer ajustes y correcciones para minimizar el tiempo de atención de ciertas fallas.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 UI">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Servicios de Elevadores /
                                <b>
                                    Evaluación de transportación vertical en edificios en uso
                                </b>
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/6.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Para edificios en uso, contamos con el servicio de evaluación de transportación vertical. En dicho estudio se analiza la calidad de la transportación vertical actual       y se proponen alternativas de solución a problemas existentes en el edificio.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Estudio de tráfico
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/7.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    El estudio de tráfico tiene por objeto determinar las características y cantidad de elevadores necesarios para realizar una transportación vertical óptima en el edificio.
                                    Este estudio de tráfico se basa en estándares internacionales y toma en consideración población, numero de pisos y clasificación del edificio. Los resultados que el estudio de tráfico arroja, son la velocidad, cantidad y capacidad de los elevadores.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Análisis de tráfico
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/8.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    Es un servicio adicional para determinar la calidad de la transportación vertical en edificios ya habitados y proponer ideas, equipo o logística para mejorar.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                Apoyo teléfonico / Correo electrónico
                            </p>
                            <figure class="img-container">
                                <img alt="single" class="img-responsive" src="images/servicios/10.jpg"/>
                            </figure>
                            <ul class="tags-list">
                                <li style="text-align: justify;">
                                    En cualquier momento nos puede llamar o escribir para juntos revisar sus necesidades de puertas automáticas o responder dudas que tenga con respecto a nuestro equipo o su instalación. Seguramente nuestro departamento comercial podrá responder a cualquiera de sus preguntas.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
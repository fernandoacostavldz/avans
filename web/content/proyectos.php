<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyectos
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Galería de Proyectos
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Proyectos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Live_Aqua.php">
                                <p class="post-title base-text-color">
                                    Grand Fiesta Americana
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/LiveAqua.jpg"/>
                                    <!--<div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Live_Aqua.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Elevadores recomendados en edificios donde es factible colocar un cuarto de máquinas en la azotea del mismo.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Fashion_Drive.php">
                                <p class="post-title base-text-color">
                                    Fashion Drive
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/FashionDrive.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Fashion_Drive.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Elevadores que optimizan el espacio al no requerir cuarto de máquinas y desplazarse en un cubo más reducido.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Arboleda.php">
                                <p class="post-title base-text-color">
                                    Arboleda
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Arboleda.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Arboleda.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Restaurante.php">
                                <p class="post-title base-text-color">
                                    Restaurante
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Restaurante.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Restaurante.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Uso en aplicaciones industrial y doméstico para el transporte de material entre diferentes niveles.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 idnetity">
                        <div class="project-item post-item">
                            <a href="Equus335.php">
                                <p class="post-title base-text-color">
                                    Equus 335
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus335.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Equus335.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Para todo tipo edificio con alto volumen de tráfico entre pisos.
                                        <br>
                                        <br>
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Holy_Cow.php">
                                <p class="post-title base-text-color">
                                    Holy Cow
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/HolyCow.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Holy_Cow.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Uso en aplicaciones industriales para el transporte de materiales entre diferentes niveles.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Equus444.php">
                                <p class="post-title base-text-color">
                                    Equus 444
                                    <br>
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/Equus444.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Equus444.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="Trebol_Park.php">
                                <p class="post-title base-text-color">
                                    Trebol Park
                                    <br>
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" src="images/Portadas1/TrebolPark.jpg"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="Trebol_Park.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 col-md-4 identity">
                        <div class="project-item post-item">
                            <a href="PabellonM.php">
                                <p class="post-title base-text-color">
                                    Pabellon M
                                    <br>
                                </p>
                                <figure class="img-container">
                                    <img alt="single" class="img-responsive" height="200px" src="images/Portadas1/PabellonM.jpg" width="270px"/>
                                    <!-- <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="PabellonM.php">
                                            </a>
                                        </div>
                                    </div> -->
                                </figure>
                                <ul class="tags-list">
                                    <li>
                                        Para todo tipo edificio con alto volumen de trafico entre pisos.
                                    </li>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Nuestros Clientes
                    </h2>
                </div>
                <div class="logos-carousel logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/1.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/2.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/3.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/4.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/5.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/logo 168x148/6.png"/>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
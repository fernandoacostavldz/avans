<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Arboleda
    </title>
    <?php include "includes/scripts_top.php"; ?>        
    <body>
        <?php include "includes/header.php"; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Arboleda
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left:50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Ventajas
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Accesos automáticos para facilitar el acceso a personas de cualquier edad y condición física
                                </li>
                                <li>
                                    Diseño rustico vanguardista
                                </li>
                                <li>
                                    Componentes y perfiles importados
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Descripción del Proyecto
                            </h2>
                            <p style="text-align: justify;">
                                Es un proyecto innovador y flexible cuenta con una Casa Club con salón de eventos sociales en el que se puede realizar eventos casuales, familiares y de negocio.
                                <br> 
                                Con la idea de conservar el diseño arquitectónico, en este proyecto se utilizaron puertas con hojas de madera modelo Slide, montadas de manera superficial. 
                                <br>
                                Estos equipos cuentan con componentes y perfiles de aluminio importados, ensambladas localmente. Se aplicó pintura electrostática a todos los perfiles de aluminio para igualar al resto de la fachada.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/arboleda/wide2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:0,
                    alignX:.60,
                },
                {
                    url:"images/proyectos/puertas/arboleda/wide3.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:0,
                    alignX:0,
                },                     
                {
                    url:"images/proyectos/puertas/arboleda/wide4.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:1,
                    alignX:0,
                },                     
                {
                    url:"images/proyectos/puertas/arboleda/wide5.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.8,
                    alignX:.5,
                },                     
                {
                    url:"images/proyectos/puertas/arboleda/wide6.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.1,
                    alignX:0,
                },                     
                {
                    url:"images/proyectos/puertas/arboleda/wide1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.5,
                    // alignX:0,
                },                     
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
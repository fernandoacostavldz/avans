<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Trebol Park
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                The Capital Grille
            </h2>
        </section>
    
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Equipo: Avans
                                </li>
                                <li>
                                    Modelo: Slide
                                </li>
                                <li>
                                    Montura: Dentro del marco / O-X-X-O
                                </li>
                                <li>
                                    Hojas: Madera
                                </li>
                                <li>
                                    Perfil: Nacional
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Automatización de una puerta de madera
                            </h2>
                            <p style="text-align: justify;">
                             En este proyecto, el reto de Avans fue lograr la automatización de una puerta de madera de seis metros de ancho, que el cliente suministró y que ya había adquirido con anticipación.
                             <br>
                             A esta puerta se le tuvo que instalar un operador. La ventaja de los operadores es que se pueden ajustar a las necesidades de cada cliente.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/capital_grille/capital1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                },
                {
                    url:"images/proyectos/puertas/capital_grille/capital2.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.23,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
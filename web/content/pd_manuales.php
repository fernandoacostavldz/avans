<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en"> <!--<![endif]-->
    <title>Avans - Manuales ICU CCU</title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l"><b>Puerta deslizante:</b> Manuales ICU CCU</h2>

                    <ul id="breadcrumbs" class="fl-r">
                        <li><a href="index.php">Inicio</a></li>
                        
                        <li>Productos</li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div id="project-slider_big" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-1.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-2.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-3.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-4.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-5.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-6.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-7.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-8.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-9.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 770x600/DEZLIZANTES/manuales-icu-ccu/ccu-10.jpg" alt="single" /></div>
                            </div>

                            <div id="project-slider_small" class="project-slider">
                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-1.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-2.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-3.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-4.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-5.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-6.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-7.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-8.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-9.jpg" alt="single" /></div>

                                <div><img src="images/puertas/PUERTAS 170x140/DEZLIZANTES/manuales-icu-ccu/ccu-10.jpg" alt="single" /></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4">
                        <aside id="sidebar" class="project-info">
                            <h2>Características</h2>
                            <ul>
                                <li> 
                                    <p>
                                        La puerta CGL LT es nuestro modelo minimalista en su diseño. Diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia. Los vidrios templados con cantos pulidos y abrillantados de forma perimetral están unidos a hueso contra el piso y costados, lo que da una sensación de transparencia y modernidad incomparable. 
                                    </p>

                                    <p>
                                        La puerta CGL LT cuenta con una batería de respaldo que acciona la puerta en caso de falta de energía y un cerrojo electrónico no visible totalmente en línea con la estética limpia del sistema.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otras_puertas.php'; ?>
        <?php include 'includes/footer.php' ?>
        <?php include 'includes/scripts_bottom.php' ?>
        <?php include "chat.php"; ?>
    </body>
</html>
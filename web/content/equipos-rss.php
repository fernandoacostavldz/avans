<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Equipos Residenciales Silla Salvaescaleras
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Equipos Residenciales Silla Salvaescaleras
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="intro-section text-center" id="intro-fullwidth">
            <div class="owlSliderImg" data-animout="fadeOut" data-auto="true" data-autospeed="3000" data-dots="false" data-items="1" data-loop="true" data-mousedrag="false" data-nav="true" id="intro-media">
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/banner-silla-salvaescaleras.jpg">
                </div>
                <div class="slide-backstretch" data-backstretch-img="images/productos 1170x600/modelo1-salvaescaleras.jpg">
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="project-content">
                            <h4 class="project-title">
                                Para todo tipo edificio con alto volumen de trafico entre pisos.
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características
                            </h2>
                            <h4>
                                - Silla con asiento ajustable y giratorio.
                                <br/>
                                - Para todo tipo de escalera.
                                <br/>
                                - Para uso comercial.
                                <br/>
                                - Uso para personas con capacidades diferentes.
                                <br/>
                            </h4>
                            <br/>
                            <br/>
                            <h2>
                                Características Opcionales
                            </h2>
                            <h4 class="project-title">
                                - Dimensiones de Cabina: 1.00 x 1.30 mts.
                            </h4>
                            <h4 class="project-title">
                                - No requiere cuarto de máquinas
                            </h4>
                            <h4 class="project-title">
                                - Voltaje de operación monofácico
                            </h4>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Puertas Deslizantes
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Puertas Deslizantes
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="pd_marcoaluminio.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Marco de Aluminio
                                        <br>
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/-/pd-marco-aluminio.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container"> -->
                                            <!-- <a class="icon-plus" href="pd_marcoaluminio.php"> -->
                                            <!-- </a> -->
                                        <!-- </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        El sistema de sensores Ultraview, exclusivo de BESAM, es el más seguro, moderno y estilizado del mercado.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="pd_totalmentecristal.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Totalmente Cristal
                                        <br>
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/1170x600/--/pd-totalmente-cristal-2.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_totalmentecristal.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">La puerta CGL LT esta diseñada para lugares donde se desea minimizar la exposición de aluminio y maximizar la transparencia.</p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_zocloinferior.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Zoclo Inferior
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/-/pd-zoclo-inferior.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_zocloinferior.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        El nuevo modelo CGL es una puerta elegante, silenciosa y robusta.
                                        <br>
                                        <br>
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_telescopica.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Telescópicas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/-/pd-telescopica.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_telescopica.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        La puerta Unislide es ideal para lugares de alto flujo donde pueda estar expuesto a golpes de los usuarios.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_macropuertas.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Macro Puertas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/1170x600/--/pd-macropuertas.jpg"/>
                                    <div class="post-item__description">
                                      <!--   <div class="link-container">
                                            <a class="icon-plus" href="pd_macropuertas.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Contamos con nuestro departamento de Ingeniería para diseñar cualquier modelo de puerta automática que se tenga en mente.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_cuartolimpio.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Cuarto Limpio
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/-/pd-cuarto-limpio.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_cuartolimpio.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Incluye junquillos a 45 grados, lo que reduce el almacenamiento de polvo. Felpas de Santopreno especializadas, permite sellar el cuarto del paso de aire.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_art.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Artísticas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/1170x600/--/pd-artisticas1.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_art.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        En línea con nuestra filosofía de innovación y servicio, podemos hacer cualquier modelo que se tenga en mente.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_manuales.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Manuales ICU CCU
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/1170x600/--/pd-manuales-ICU-CCU1.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pd_manuales.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        En línea con nuestra filosofía de innovación y servicio, podemos hacer cualquier modelo que se tenga en mente.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                  <!--   <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pd_tp.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Transporte Público
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/-/pd-transporte-publico1.jpg"/>
                                    <div class="post-item__description">
                                        <div class="link-container">
                                            <a class="icon-plus" href="pd_tp.php">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Carriles Exclusivos para autobuses, el objetivo es combinar los carriles de autobuses como 'estaciones' de autobuses de alta calidad.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
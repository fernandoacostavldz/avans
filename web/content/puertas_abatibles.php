<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Puertas Abatibles
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        Puertas Abatibles
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="pa_hd.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Hojas Dobles
                                        <br/>
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Abatibles/pa-hojas-dobles.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pa_hd.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        El modelo Swingmaster MP presenta la característica de tener un operador electromecánico silencioso y robusto. Nuestro operador transmite la energía a través de un sistema de engranes y resortes.
                                        <br/>
                                        <br/>
                                        El operador se integra a través de un brazo de acero reforzado a la puerta de aluminio de forma invisible para el usuario. Esta puerta es ideal para lugares de alto flujo como pasillos o puertas principales.dor electromecánico silencioso y robusto
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 UI">
                        <div class="project-item post-item">
                            <a href="pa_hs.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Hojas Sencillas
                                        <br/>
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Abatibles/pa-hojas-sencillas.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pa_hs.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        El modelo Swingmaster MP presenta la característica de tener un operador electromecánico silencioso y robusto. Nuestro operador transmite la energía a través de un sistema de engranes y resortes.
                                        <br/>
                                        <br/>
                                        El operador se integra a través de un brazo de acero reforzado a la puerta de aluminio de forma invisible para el usuario. Esta puerta es ideal para lugares de alto flujo como pasillos o puertas principales.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <a href="pa_opun.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Operador Únicamente
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Abatibles/pa-operador-unicamente2.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pa_opun.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Nuestro operador Power Swing es hidráulico de baja energía, ideal para modernizaciones donde ya se cuenta con una puerta abatible manual y lo único que se requiere es automatizarla. Su operador hidráulico presenta la oportunidad de operar la puerta de forma manual, si se desea, sin dañar los componentes internos del sistema.
                                        <br/>
                                        <br/>
                                        El operador está diseñado para montarse superficialmente sobre un muro o una placa metálica. El operador Power Swing es muy utilizado en acceso a baños donde contar con una puerta automática elimina la necesidad de tener contacto con la perilla.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 interactive">
                        <div class="project-item post-item">
                            <a href="pa_comp.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Compartimentalizadoras
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Abatibles/pa-compartimentalizadoras.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus" href="pa_comp.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Nuestro modelo de puerta compartimentalizadora integra un operador con resistencia certificada a fuego de 90 minutos, así como hojas con marco de acero panel de abeja y mirillas especializadas sin malla interior con la misma resistencia.
                                        <br/>
                                        <br/>
                                        Comúnmente utilizada en salidas de emergencia o áreas del edificio que requieran compartimentalización, este equipo cumple con las exigencias internacionales en materia de seguridad y contención de incendios.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Iza Latitud Polanco
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Iza Latitud Polanco
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>Características</li>
                                <li>Equipo: Avans</li>
                                <li>Modelo: Glass LT</li>
                                <li>Montura: Dentro del marco / P-X</li>
                                <li>Vidrio: Templado / Claro / 9mm</li>
                                <li>Acabado: Anodizado Natural Claro</li>
                                <li>Perfil: Nacional</li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Accesos seguros
                            </h2>
                            <p style="text-align: justify;">
                                Se instalaron puertas automáticas deslizantes para entrar a las oficinas generales de Iza, cuentan con cerrojo electrónico y control de acceso a través de una tarjeta. 
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
        <script type="text/javascript">
            $(function(){
                $("#backstretch-photo").backstretch([
                    {
                        // url:"/images/proyectos/pabellonm/pabellonm.jpg?width={width}&height={height}",
                        url:"/images/proyectos/puertas/iza_latitud_polanco/ILP_1000.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.2,
                        // alignX:,
                    },
                    {
                        url:"/images/proyectos/puertas/iza_latitud_polanco/ILP2_1000.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.17
                    },                     
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
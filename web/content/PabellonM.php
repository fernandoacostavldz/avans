<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Pabellon M
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Pabellón M
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Espectáculos de primera
                            </h2>
                            <p style="text-align: justify;">
                                Es un desarrollo inmobiliario espectacular y único, cuenta con oficinas, centro de convenciones, centro comercial, restaurantes, hotel, Foro DiDi y un auditorio.
                                <br>
                                En el auditorio Pabellón M, Avans instaló elevadores de uso rudo, porque el recinto tiene la necesidad de transportar equipos de luz, sonido, escenario teatral y demás accesorios voluminosos propios de los espectáculos masivos.
                                <br>
                                El estudio de tráfico y la instalación de los elevadores llevó un tiempo aproximado de 12 meses. Lo que garantizó al 100% la satisfacción del cliente.
                                <br>
                                Norma Americana ASME B20.1
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 3</li>
                                        <li>Velocidad:0.20 m/s </li>
                                        <li>Capacidad: 1000 a 6000 kg</li>
                                        <li>Diseño:  Carga</li>
                                        <li>Puertas: A todo lo ancho de las plataformas 1.50 mts y 4.00 mts</li>
                                        <li>Pardas: 2 a 4</li>
                                        <li>Con motor en la parte superior</li>

                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/pabellon_m/pm-1400.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.7,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
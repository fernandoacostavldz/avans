<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Proyecto Torre Galia
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Torre Galia
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <!-- <div class="row"> -->
                    <div class="col-xs-12 col-sm-6 col-md-7" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                A la medida conservando su diseño
                            </h2>
                            <p style="text-align: justify;">
                                En este complejo habitacional ubicado en San Pedro, el reto para Avans fue fabricar e instalar elevadores de pasajeros en espacios reducidos que fueran seguros, sin perder la funcionalidad y la calidad en la transportación vertical. El edificio, de 18 pisos, necesitaba un ancho mínimo de 1.80 de fondo y que los elevadores fueran panorámicos.
                                <br>
                                La norma vigente en este proyecto es la mexicana NOM-053-053SCFI-2000 y Norma Europea EN 81-20/50
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <p class="project-title">
                                        Elevadores
                                    </p>
                                    <ul style="list-style-type: circle;">
                                        <li>Cantidad: 2</li>
                                        <li>Velocidad: 1.75 m/s</li>
                                        <li>Capacidad: 800 a 1000 kg</li>
                                        <li>Diseño: Pasajeros</li>
                                        <li>Altura de puertas: 2.10 mts</li>
                                        <li>Paradas: 18</li>
                                        <li>Tipo: Con cuarto de máquinas</li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <?php include 'includes/otros_proyectos_elevadores.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/elevadores/TOP/top_prueba.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.45,
                },
                {
                    url:"images/proyectos/elevadores/TOP/TOP2-edited-bkp.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.525,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
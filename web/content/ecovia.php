<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Ecovía
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Ecovía
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Beneficios
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>Ahorro de energía.</li>
                                <li>Puerta esclusa en la entrada principal de cada estación para evitar la fuga de aire acondicionado.</li>
                                <li>Seguridad para los pasajeros porque esperan los autobuses con las puertas cerradas.</li>
                                <li>Sensores de comunicación entre puertas y autobuses para que éstas se abran únicamente cuando el autobús está en la posición correcta.</li>
                                <li>Puertas con abatimiento para que pueda evacuar pasajeros en caso de emergencia.</li>
                                <li>Una torreta y un botón para avisarle al conductor que en la estación está presente una persona con capacidades especiales.</li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style="font-weight: 300;">
                                Puertas abatibles bidireccionales
                            </h2>
                            <p style="text-align: justify;">
                              La Ecovía es un sistema de transporte metropolitano en Monterrey, conocido como BRT (Bus Rapid Transit) por sus siglas en inglés, cuenta en total con 41 estaciones, 39 de ellas intermedias y 2 terminales.
                              <br>
                              <br>
                              En este proyecto Avans apoyó en el diseño de las puertas para todas las estaciones incluyendo las terminales. Se instalaron un total de 256 puertas, que pueden ser monitoreadas, y controladas, de forma remota.
                              <br>
                              <br>
                              Las puertas cuentan con sensores que permiten la apertura automática al sincronizarse con las unidades de transporte. De éstas, 170 son telescópicas de 6 hojas (4 hojas deslizantes y 2 fijas) que permiten un claro transitable 
                              de hasta 2.10 mts de ancho, 72 telescópicas de 3 hojas (2 hojas deslizantes y 1 fija) y el resto abatibles.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
        <script type="text/javascript">
            $(function(){
                $("#backstretch-photo").backstretch([
                    {
                        // url:"/images/proyectos/pabellonm/pabellonm.jpg?width={width}&height={height}",
                        url:"/images/proyectos/puertas/ecovia/EV_1400.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.45,
                    },
                    {
                        url:"/images/proyectos/puertas/ecovia/EV2_1400.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.5
                    },                     
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
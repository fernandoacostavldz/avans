<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Proyecto Pabellón M
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Pabellón M
            </h2>
        </section>
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Ventajas
                            </h2>
                            <ul style="list-style-type: circle;">
                                <li>
                                    Puertas resistentes a fuertes vientos
                                </li>
                                <li>
                                    Abatimiento de emergencia en todas sus hojas. 
                                </li>
                                <li>
                                    Ahorro de energía
                                </li>
                                <li>
                                    Agilidad en los accesos
                                </li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Puertas contra huracanes
                            </h2>
                            <p style="text-align: justify;">
                              Aunque Monterrey no está en una zona de huracanes, la madre naturaleza es muy impredecible y Monterrey ya ha sufrido los estragos de huracanes peligrosos como Gilberto y Alex.
                              <br>
                              Esta es una de las razones del porque en Pabellón M decidieron instalar una puerta deslizante contra huracanes. Esta puerta modelo Hurricane Resistant Sliding resiste vientos de hasta 250 km/hr y tienen un alma de acero dentro de los perfiles de aluminio cuentan con junquillos sólidos para mayor resistencia.
                              <br>
                              También se instalaron macro puertas giratorias, las cuales por su diseño elegante y vistoso le dan un plus al edificio, esto aunado al ahorro de energía que este tipo de puertas genera.
                              <br>
                              Su automatización permite entrar y salir a un número grande de personas sin necesidades de tocar las puertas, mejorando la higiene y haciendo más ágil el acceso.

                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
        <script type="text/javascript">
            $(function(){
                $("#backstretch-photo").backstretch([
                    {
                        // url:"/images/proyectos/pabellonm/pabellonm.jpg?width={width}&height={height}",
                        url:"/images/proyectos/puertas/pabellonm/pabellonm.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.45,
                    },
                    {
                        url:"/images/proyectos/puertas/pabellonm/pabellonm4.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.5
                    },                     
                    {
                        url:"/images/proyectos/puertas/pabellonm/pabellonm2.jpg",
                        transition:'fade',
                        scale:'cover',
                        fade:1000,
                        alignY:.45
                    }, 
                  ],{
                    duration:3500,
                  }
                );
            });
        </script>
    </body>
</html>
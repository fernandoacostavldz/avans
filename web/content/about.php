<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Nosotros
    </title>
    <?php include "includes/scripts_top.php"; ?>
    <body>
        <?php include "includes/header.php"; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Conoce Nuestra Empresa
                        </b>
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Empresa
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="about">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <div class="section-title">
                            <h2>
                                <b>
                                    Quiénes somos
                                </b>
                            </h2>
                        </div>
                        <p>
                            El objetivo principal de nuestra empresa es el ofrecer sistemas innovadores de entrada que faciliten el flujo de personas hacia los edificios que habitamos.

                            Es vital para nuestra empresa el mantener las puertas automáticas que instalamos en perfectas condiciones por lo que ofrecemos servicio de mantenimiento a puertas instaladas por nosotros y a equipos de cualquier otra marca.
                            <p>
                                Somos los distribuidores exclusivos de la marca BESAM en México. BESAM es el líder en la industria de puertas automáticas con mas de 1,000,000 de equipos en operación desde 1962. BESAM pertenece al grupo ASSA ABLOY; conglomerado sueco enfocado a proveer soluciones de acceso en general.

                             Hemos desarrollado relaciones con fabricantes líderes en sus áreas para ofrecer los mejores productos al mercado Mexicano. Cada uno de los productos que ofrecemos ha sido seleccionado bajo criterios estrictos de calidad y confiabilidad.
                            </p>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4">
                        <div class="title-with-ico">
                            <i class="ico icon-award rounded_50 base-text-color base-border-color">
                            </i>
                            <h3>
                                <b>
                                    Certificados en Calidad
                                </b>
                            </h3>
                        </div>
                        <p class="base-text-color" style="font-size: 18px;">
                            Tecnología y Diseño
                        </p>
                        <p>
                            El dominio de nuestras tecnologías nos permite uniformizar las marcas internacionales que presentamos, contamos con una tecnología apoyada en una fabrica de controladores operando en Monterrey, N.L.
                        </p>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
            </div>
        </section>
        <br>
        <br>
        <br>
        <section>
            <div class="container">
                <div class="row">
                    <div class="element col-xs-12 identity interactive">
                        <center>
                            <div class="video-responsive">
                                <iframe align="middle" allowfullscreen="" frameborder="0" height="480" src="https://www.youtube.com/embed/VbGMmRqk34g" width="720">
                                </iframe>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </section>
<!--         <section class="clients base-bg-color_light">
            <div class="container">
                <div class="section-title">
                    <h2>
                        Nuestros Proveedores
                    </h2>
                </div>
                <div class="logos-carousel logos-carousel_long dots-top">
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                    <div class="slide">
                        <img alt="single" src="images/clients_img/9.png"/>
                    </div>
                </div>
            </div>
        </section> -->
        <section class="infoline infoline__dark">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-8">
                        <h2>
                            Nuestras ventajas superan a la competencia
                        </h2>
                        <p>
                            Contamos con un nivel único de calidad que cuidamos en cada uno de nuestros procesos, asegurando que cada una de nuestras propuestas tengan el mejor rendimiento.
                        </p>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </section>
        <section class="services services__dark">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-ok rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    No tenemos límites / Diseño a medida
                                </h3>
                            </div>
                            <p>
                                Nuestra aportación es la solución a sus proyectos respetando sus dimensiones, capacidades y especificaciones.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-mobile rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Tiempo de Entrega
                                </h3>
                            </div>
                            <p>
                                En equipo standard tenemos los mejores plazos de entrega.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-paper-plane rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Garantía
                                </h3>
                            </div>
                            <p>
                                A traves de nuestra empresa ofrecemos la garantía de mayor confianza, con el apoyo directo de la fábrica.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-wrench-1 rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Asesoría técnica y comercial
                                </h3>
                            </div>
                            <p>
                                Ofrecemos asesoría a su proyecto desde el diseño arquitectónico hasta la logística de operación de los elevadores en los edificios.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="services-item">
                            <div class="title-with-ico">
                                <i class="ico icon-mobile rounded_50 base-text-color base-border-color">
                                </i>
                                <h3>
                                    Servicio de partes y refacciones
                                </h3>
                            </div>
                            <p>
                                Variedad y existencia de partes en la plaza.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="infoline infoline__dark infoline__dark__bg infoline__style2">
            <div class="container">
                <h2>
                    <b>
                        Únete a nuestro equipo de trabajo.
                    </b>
                </h2>
                <a class="flat-btn base-bg-color" href="unete-equipo.php">
                    Más información
                </a>
            </div>
        </section>
<!--         <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="footer-item">
                            <h4>
                                Nuestra Filosofía
                            </h4>
                            <p>
                                Texto pendiente. Accomodations trust works tighter just. Chance  however dry pennies effective. Deeply soaking below genuine bold care burst. Reduced unique win.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="footer-item">
                            <h4>
                                Misión
                            </h4>
                            <div class="tweet">
                                <article>
                                    <p>
                                        Texto pendiente. Accomodations trust works tighter just. Chance  however dry pennies effective. Deeply soaking below genuine bold care burst. Reduced unique win.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="footer-item">
                            <h4>
                                Visión
                            </h4>
                            <div class="tweet">
                                <article>
                                    <p>
                                        Texto pendiente. Accomodations trust works tighter just. Chance  however dry pennies effective. Deeply soaking below genuine bold care burst. Reduced unique win.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer> -->
        <?php include "includes/footer.php"; ?>
        <?php include "includes/scripts_bottom.php"; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
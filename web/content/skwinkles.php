<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Planta Skwinkles
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <!-- SLIDE SHOW -->
            <div id="backstretch-photo">
            </div>
        <!-- / SLIDE SHOW -->
        <section class="title-bottom-picture">
            <h2 class="fl-l">  
                Planta Skwinkles
            </h2>
        </section>
    
        <section id="content-container" style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left: 50px;">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul style="list-style-type: circle;">
                            	<li>
                            		Equipo: Avans
                            	</li>
                            	<li>
                            		Modelo: Slide
                            	</li>
                            	<li>
                            		Montura: Dentro del marco / O-X-X-O
                            	</li>
                            	<li>
                            		Vidrio: Templado / Claro / 9mm
                            	</li>
                            	<li>
                            		Acabado: Anodizado Natural Mate
                            	</li>
                            	<li>
                            		Perfil: Nacional
                            	</li>
                            </ul>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8" id="project-container">
                        <div class="project-content">
                            <h2 class="project-title" style=" font-weight: 300;">
                                Mayor seguridad e higiene de la planta
                            </h2>
                            <p style="text-align: justify;">
                                En esta planta, que fabrica dulces, chicles y confitería, se instalaron en total 11 puertas. Las áreas beneficiadas con la instalación de estas puertas son: cocina, oficinas y acceso a la planta entre otras.
                                <br>
                                Entre las características de las puertas destacan vidrio templado de 9mm y acabado natural mate.
                            </p>
                        </div>
                    </div>
                    <aside class="map-info-below" id="sidebar">
                    </aside>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_proyectos.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
    <script type="text/javascript">
        $(function(){
            $("#backstretch-photo").backstretch([
                {
                    url:"images/proyectos/puertas/skwinkles/skwinkles1.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.6,
                    // alignX:.9,
                },
                {
                    url:"images/proyectos/puertas/skwinkles/skwinkles3.jpg",
                    transition:'fade',
                    scale:'cover',
                    fade:1000,
                    alignY:.9,
                    // alignX:.5,
                },
              ],{
                duration:3500,
              }
            );
        });
    </script>
</html>
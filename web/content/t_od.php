<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <!--<![endif]-->
    <title>
        Avans - Torniquete Óptico Deslizante
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        <b>
                            Torniquete:
                        </b>
                        Óptico Deslizante
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <!--<li><a href="#">Caracteristicas</a></li>-->
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="content-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="project-sliders">
                            <div class="project-slider" id="project-slider_big">
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/opticos-deslizantes/torniquetes-opticos-deslizantes-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/opticos-deslizantes/torniquetes-opticos-deslizantes-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/opticos-deslizantes/torniquetes-opticos-deslizantes-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 770x600/opticos-deslizantes/torniquetes-opticos-deslizantes-4.jpg"/>
                                </div>
                            </div>
                            <div class="project-slider" id="project-slider_small">
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/opticos-deslizantes/torniquetes-opticos-deslizantes-1.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/opticos-deslizantes/torniquetes-opticos-deslizantes-2.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/opticos-deslizantes/torniquetes-opticos-deslizantes-3.jpg"/>
                                </div>
                                <div>
                                    <img alt="single" src="images/TORNIQUETES 170x140/opticos-deslizantes/torniquetes-opticos-deslizantes-4.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <aside class="project-info" id="sidebar">
                            <h2>
                                Características
                            </h2>
                            <ul>
                                <li>
                                    <p>
                                        Este torniquete permite el control discreto de su personal, al contar con una alarma visual y/o auditiva que se activa al no pasar la identificación aceptada por el sistema de control de accesos.
                                    </p>
                                    <p>
                                        Este equipo es comúnmente utilizado en lugares de alto flujo donde se tenga un guardia de seguridad que apoyaría en las labores de revisión.
                                    </p>
                                </li>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/otros_torniquetes.php'; ?>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>
<?php include "php/mail.php" ?>
<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <title>
        Avans - Puertas Giratorias
    </title>
    <?php include 'includes/scripts_top.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <section id="headline">
            <div class="container">
                <div class="section-title clearfix">
                    <h2 class="fl-l">
                        Puertas Giratorias
                    </h2>
                    <ul class="fl-r" id="breadcrumbs">
                        <li>
                            <a href="index.php">
                                Inicio
                            </a>
                        </li>
                        <li>
                            Productos
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="portfolio-container">
            <div class="container">
                <div class="row isotope-container" id="isotope-default">
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="pg_dh.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Dos Hojas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-dos-hojas.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus"  href="pg_dh.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Nuestro modelo Two Wing es imponente por naturaleza. Su gran dimensión permite incrementar el flujo de personas de forma natural protegiendo el edificio de la transferencia de calor a través del flujo de aire – lo que trae consigo ahorros importantes en materia de costos energéticos.
                                        <br>
                                        <br>
                                        <br>
                                        Sus hojas centrales se abaten hacia el exterior en caso de pánico y su sistema de sensores maximiza la seguridad para el usuario de este equipo.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 identity">
                        <div class="project-item post-item">
                            <a href="pg_th.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Tres Hojas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-tres-hojas.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus"  href="pg_th.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Nuestra puerta giratoria de tres hojas permite al usuario transitar con maletas e inclusive sillas de ruedas. Está diseñada para cumplir con todos los estándares de seguridad y calidad internacionales.
                                        <br/>
                                        <br/>
                                        Sus tres hojas se abaten al centro en caso de pánico o emergencia, cuenta con un botón de seguridad y protección electrónica en los costados de las hojas curvas fijas, así como sensores de seguridad el eje horizontal de cada hoja.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 UI">
                        <div class="project-item post-item">
                            <a href="pg_ch.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Cuatro Hojas
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-cuatro-hojas.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus"  href="pg_ch.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        El modelo Four Wing, es el más utilizado en lugares donde el paso de personas es constante. Permite el flujo de personas a la entrada y salida de su edificio, lo que incrementa el tránsito considerablemente.
                                        <br/>
                                        <br/>
                                        Su sistema de seguridad es el más avanzado del mercado, integrando sensores, switches de compresión y botones de emergencia.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 mobile">
                        <div class="project-item post-item">
                            <p class="post-title base-text-color">
                                <b>
                                    Manuales
                                </b>
                            </p>
                            <div class="img-container">
                                <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-manuales.jpg"/>
                                <div class="post-item__description">
                                </div>
                            </div>
                            <ul class="tags-list">
                                <p style="color:#4B454B">
                                    Contamos con puertas giratorias manuales que se adaptan a cualquier diseño en mente. Algunos de nuestros equipos tienen mas de 80 años en operación, prueba de su alta calidad y especificación en su diseño y producción.
                                    <br/>
                                    <br/>
                                    Estos equipos son recomendados para lugares con fuertes cargas de viento donde la puerta giratoria aísle la entrada del flujo de aire.
                                </p>
                            </ul>
                        </div>
                    </div>
                    <div class="element col-xs-12 col-sm-6 interactive">
                        <div class="project-item post-item">
                            <a href="pg_as.php">
                                <p class="post-title base-text-color">
                                    <b>
                                        Alta Seguridad
                                    </b>
                                </p>
                                <div class="img-container">
                                    <img alt="single" class="img-responsive" src="images/puertas/PuertasIII/570x300/Giratorias/pg-alta-seguridad1.jpg"/>
                                    <div class="post-item__description">
                                        <!-- <div class="link-container">
                                            <a class="icon-plus"  href="pg_as.php">
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                                <ul class="tags-list">
                                    <p style="color:#4B454B">
                                        Normalmente, las puertas giratorias pueden permitir el paso de dos o más personas a la vez en cada compartimiento, lo cual puede representar una violación a la seguridad de ciertos edificios. Nuestra puerta RD4A asegura, a través de nuestro Sensor Secure360, que únicamente UNA persona pase a la vez en cada compartimiento. Por lo tanto, en entradas a edificios corporativos que requieren alta seguridad y que cuentan con control de acceso a través de Credenciales de Identificación, nuestra puerta RD4A asegurará que únicamente UNA persona pase con una credencial, brindando un nivel de seguridad incomparable.
                                        <br/>
                                        <br/>
                                        Cuando dos personas tratan de pasar con una misma credencial, ya sea una detrás de la otra o bien una siendo cargada por la otra, la puerta detectará esta violación de seguridad, parará en el compartimiento seguro, generará una alarma y se regresará automáticamente evitando así la entrada de dos personas a la zona segura del edificio.
                                    </p>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts_bottom.php'; ?>
        <?php include "chat.php"; ?>
    </body>
</html>